<?php
use Cake\ORM\TableRegistry;
use App\Event\DriversListener;
use App\Event\InvoicesListener;

TableRegistry::getTableLocator()->get('DriversOrders')
    ->getEventManager()
    ->on('Model.afterSave', function($event, $entity)
    {
        $driverListner = new DriversListener();
        $driverListner->afterSave($event,$entity);
    });

TableRegistry::getTableLocator()->get('Orders')
    ->getEventManager()
    ->on('Model.afterSave', function($event, $entity)
    {
        $InvoicesListener = new InvoicesListener();
        $InvoicesListener->OrdersSaved($event,$entity);
    });

TableRegistry::getTableLocator()->get('UsersManager.Users')
    ->getEventManager()
    ->on('Model.afterSave', function($event, $entity)
    {
        $InvoicesListener = new InvoicesListener();
        $InvoicesListener->UserSaved($event,$entity);
    });
