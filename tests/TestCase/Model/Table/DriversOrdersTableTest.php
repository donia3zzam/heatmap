<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DriversOrdersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DriversOrdersTable Test Case
 */
class DriversOrdersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\DriversOrdersTable
     */
    protected $DriversOrders;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.DriversOrders',
        'app.Drivers',
        'app.Orders',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DriversOrders') ? [] : ['className' => DriversOrdersTable::class];
        $this->DriversOrders = TableRegistry::getTableLocator()->get('DriversOrders', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->DriversOrders);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
