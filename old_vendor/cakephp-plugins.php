<?php
$baseDir = dirname(dirname(__FILE__));
return [
    'plugins' => [
        'ADmad/I18n' => $baseDir . '/vendor/admad/cakephp-i18n/',
        'Bake' => $baseDir . '/vendor/cakephp/bake/',
        'BootstrapUI' => $baseDir . '/vendor/friendsofcake/bootstrap-ui/',
        'DebugKit' => $baseDir . '/vendor/cakephp/debug_kit/',
        'Media' => $baseDir . '/plugins/Media/',
        'Menus' => $baseDir . '/plugins/Menus/',
        'Migrations' => $baseDir . '/vendor/cakephp/migrations/',
        'Notifictions' => $baseDir . '/plugins/Notifictions/',
        'Settings' => $baseDir . '/plugins/Settings/',
        'UsersManager' => $baseDir . '/plugins/UsersManager/',
        'Utils' => $baseDir . '/plugins/Utils/',
        'WyriHaximus/TwigView' => $baseDir . '/vendor/wyrihaximus/twig-view/'
    ]
];