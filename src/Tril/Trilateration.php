<?php
/*
 * This file is part of the Trilateration package.
 *
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * @author Matias Gutierrez <soporte@tbmsp.net>
 * @file Trilateration.php
 *
 * Project home:
 *   https://github.com/tbmsp/trilateration
 *
 */
//namespace net\TBMSP\Code\Trilateration;
namespace App\Tril;
class Trilateration{
    const DistenceFactor = -46;
    const solidNumber = 25;
    var $methods = ['Old','New','Mixed'];
    var $method = "New";
    var $ignore = 70;
    var $envirmentFactor = 25;
    var $baseRSSI = -46;

    function Compute($p1, $p2, $p3){
        $dist = 18.5;
        $xa = $p1['sensor_x']/100;
        $ya = $p1['sensor_y']/100;
        $xb = $p2['sensor_x']/100;
        $yb = $p2['sensor_y']/100;
        $xc = $p3['sensor_x']/100;
        $yc = $p3['sensor_y']/100;

        $p1['r'] = (int)$p1['r'];
        $p2['r'] = (int)$p2['r'];
        $p3['r'] = (int)$p3['r'];

        if($this->method == "Old"){

            $ra = floor(($dist - ( (int)$p1['r'] * $dist /100 ) )*100);
            $rb = floor(($dist - ( (int)$p2['r'] * $dist /100 ) )*100);
            $rc = floor(($dist - ( (int)$p3['r'] * $dist /100 ) )*100);
        }
        if($this->method == "New"){

            $ra = pow(10 , (($this->baseRSSI +(int)$p1['r'])/ $this->envirmentFactor) );
            $rb = pow(10 , (($this->baseRSSI +(int)$p2['r'])/ $this->envirmentFactor) );
            $rc = pow(10 , (($this->baseRSSI +(int)$p3['r'])/ $this->envirmentFactor) );
        }
        if($this->method == "Mixed"){
            if($p3['r'] > $this->ignore){
                //the old method
                $ra = floor(($dist - ( (int)$p1['r'] * $dist /100 ) )*100);
                $rb = floor(($dist - ( (int)$p2['r'] * $dist /100 ) )*100);
                $rc = floor(($dist - ( (int)$p3['r'] * $dist /100 ) )*100);
            }else{
                // the new one
                $ra = pow(10 , (($this->baseRSSI +(int)$p1['r'])/ $this->envirmentFactor) );
                $rb = pow(10 , (($this->baseRSSI +(int)$p2['r'])/ $this->envirmentFactor) );
                $rc = pow(10 , (($this->baseRSSI +(int)$p3['r'])/ $this->envirmentFactor) );
            }
        }
         $S = (pow($xc, 2) - pow($xb, 2) + pow($yc, 2) - pow($yb, 2) + pow($rb, 2) - pow($rc, 2)) / 20;
         $T = (pow($xa, 2) - pow($xb, 2) + pow($ya, 2) - pow($yb, 2) + pow($rb, 2) - pow($ra, 2)) / 20;
         $y = (($T * ($xb - $xc)) - ($S * ($xb - $xa))) / ((($ya - $yb) * ($xb - $xc)) - (($yc - $yb) * ($xb - $xa)));
         $x = (($y * ($ya - $yb)) - $T) / ($xb - $xa);

        return [
            'x'=>floor($x*10),
            'y'=>floor($y*10),
//            'p1' => $p1,
//            'p2' => $p2,
//            'p3' => $p3,


        ];

    }
    function A($a){return $a*7.2;}
    function B($a){return $a/900000;}
    function C($a,$b,$c,$d){$e=3.14159265359;return array($a*cos(($e/180)*$d)-$b*sin(($e/180)*$d),$a*sin(($e/180)*$d)+$b*cos(($e/180)*$d),$c);}
    function D($a){return 730.24198315+5233325511*$a+1.35152407*pow($a,2)+0.01481265*pow($a,3)+0.00005900*pow($a,4)+0.00541703*180;}
    function E($a,$b,$c,$d){$e=3.14159265359;$f=$e*$a/180;$g=$e*$c/180;$h=$b-$d;$i=$e*$h/180;$j=sin($f)*sin($g)+cos($f)*cos($g)*cos($i);if($j>1){$j=1;}$j=acos($j);$j=$j*180/$e;$j=$j*60*1.1515;$j=$j*1.609344;return $j;}
}
?>
