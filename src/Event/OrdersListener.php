<?php
namespace App\Event;
use Cake\Event\EventListenerInterface;
use Cake\Log\Log;


class OrdersListener implements EventListenerInterface
{
    public function implementedEvents() :array
    {
        return [
            'Model.Orders.afterSave' => 'afterSave',
            'Model.Orders.afterRemove' => 'afterRemove',
        ];
    }

    public function afterSave($event, $order)
    {
        pr($order);
        die();
    }

    public function afterRemove($event, $user)
    {
        Log::write('debug', $user['name'] . ' has deleted his/her account.');
    }
}


?>
