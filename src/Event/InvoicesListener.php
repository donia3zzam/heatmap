<?php
namespace App\Event;
use Cake\Event\EventListenerInterface;
use Cake\Log\Log;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;


class InvoicesListener implements EventListenerInterface
{
    public function implementedEvents() :array
    {
        return [
            'Model.Orders.afterSave' => 'OrdersSaved',
            'Model.Orders.afterRemove' => 'OrdersRemoved',
            'Model.Users.afterSave' => 'UserSaved',
        ];
    }

    public function OrdersSaved($event, $order)
    {
        if($order->invoice_id === null && $order->statues == "Collected"){
            //update invoice id to the latest un cleared invoice
            $invoicesTable = TableRegistry::getTableLocator()->get('Invoices');
            $ordersTable = TableRegistry::getTableLocator()->get('Orders');
            $newOrder = $ordersTable->get($order->id);
            $newOrder->invoice_id = $invoicesTable->getCurrentActiveInvoice($order->user_id)->id;
            $ordersTable->save($newOrder);
        }
    }

    public function OrdersRemoved($event, $user)
    {
        Log::write('debug', $user['name'] . ' has deleted his/her account.');
    }

    public function UserSaved($event, $user){
        if ($user->isNew()) {
            // Create the first open invoice
            $invoicesTable = TableRegistry::getTableLocator()->get('Invoices');
            $invoicesTable->CreateEmptyInvoice($user->id);
        }
    }
}


?>
