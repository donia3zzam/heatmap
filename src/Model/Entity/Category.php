<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Category Entity
 *
 * @property int $id
 * @property string $name
 * @property string $image
 * @property int|null $parent_id
 * @property string $description
 * @property string $type
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\ParentCategory $parent_category
 * @property \App\Model\Entity\Ad[] $ads
 * @property \App\Model\Entity\ChildCategory[] $child_categories
 * @property \App\Model\Entity\Field[] $fields
 */
class Category extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_virtual = ['ads_count','has_sub_categories'];

    protected $_accessible = [
        'name' => true,
        'image' => true,
        'parent_id' => true,
        'description' => true,
        'type' => true,
        'created' => true,
        'modified' => true,
        'parent_category' => true,
        'ads' => true,
        'child_categories' => true,
        'fields' => true,
    ];

    protected function _getAdsCount()
    {
        $adsTable = TableRegistry::getTableLocator()->get('Ads');
        $adcount = $adsTable->find('all')->where(['category_id' => $this->id])->count();
        return $adcount ." Ads";
    }
    protected function _getHasSubCategories()
    {
        $catsTable = TableRegistry::getTableLocator()->get('Categories');
        $subCats = $catsTable->find()->where(['parent_id' => $this->id])->count();
        if($subCats> 0  || $this->id == 59 ){
            return true;
        }
        return false;
    }
}
