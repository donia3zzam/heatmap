<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Event Entity
 *
 * @property int $id
 * @property string $name
 * @property string $location
 * @property \Cake\I18n\FrozenDate $event_start_day
 * @property \Cake\I18n\FrozenDate $event_end_day
 * @property string $logo
 * @property string|null $images
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Hall[] $halls
 * @property \App\Model\Entity\Point[] $points
 * @property \App\Model\Entity\Sensor[] $sensors
 */
class Event extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'location' => true,
        'event_start_day' => true,
        'event_end_day' => true,
        'logo' => true,
        'images' => true,
        'created' => true,
        'modified' => true,
        'halls' => true,
        'points' => true,
        'sensors' => true,
    ];
}
