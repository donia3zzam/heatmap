<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Ad Entity
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property string $image_folder
 * @property int $category_id
 * @property int $location_id
 * @property int $user_id
 * @property bool $active
 * @property int $statues
 * @property string $type
 * @property int $active_fo_days
 * @property bool $featured
 * @property int $views
 * @property int $rate
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\Location $location
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Favorite[] $favorites
 * @property \App\Model\Entity\Field[] $fields
 */
class Ad extends Entity
{
    protected $_virtual = ['images_list'];

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'slug' => true,
        'description' => true,
        'image_folder' => true,
        'category_id' => true,
        'location_id' => true,
        'user_id' => true,
        'active' => true,
        'statues' => true,
        'type' => true,
        'active_fo_days' => true,
        'featured' => true,
        'views' => true,
        'rate' => true,
        'price' => true,
        'created' => true,
        'modified' => true,
        'category' => true,
        'location' => true,
        'user' => true,
        'favorites' => true,
        'fields' => true,
    ];

    function _getImagesList()
    {
        if($this->image_folder != ""){
            $images = [];
            $handle = opendir(ROOT.DS."webroot".DS."responsive_filemanager".DS."source".DS.$this->image_folder);
            while($file = readdir($handle)){
                if($file !== '.' && $file !== '..'){
                   $images[] = ROOT_URL."/responsive_filemanager/source/".$this->image_folder."/". $file;
                }
            }
            return $images;
        }
        return [];
    }

}
