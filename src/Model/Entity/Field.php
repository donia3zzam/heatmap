<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Field Entity
 *
 * @property int $id
 * @property string $name
 * @property int $category_id
 * @property string $type
 * @property string $options
 * @property bool $required
 * @property bool $search
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\Ad[] $ads
 */
class Field extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_virtual = ['option_array'];

    protected $_accessible = [
        'name' => true,
        'category_id' => true,
        'type' => true,
        'options' => true,
        'required' => true,
        'search' => true,
        'created' => true,
        'modified' => true,
        'category' => true,
        'ads' => true,
    ];

protected function _getOptionArray(){
        $options = $this->options==null?"":$this->options;
$opt = explode('
',$options);
        return $opt;
    }
}
