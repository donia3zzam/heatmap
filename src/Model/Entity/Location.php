<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Location Entity
 *
 * @property int $id
 * @property string $name
 * @property int|null $parent_id
 * @property int $lft
 * @property int $rght
 * @property int $created
 *
 * @property \App\Model\Entity\ParentLocation $parent_location
 * @property \App\Model\Entity\Ad[] $ads
 * @property \App\Model\Entity\ChildLocation[] $child_locations
 */
class Location extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'parent_id' => true,
        'lft' => true,
        'rght' => true,
        'created' => true,
        'parent_location' => true,
        'ads' => true,
        'child_locations' => true,
    ];
}
