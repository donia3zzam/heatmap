<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Point Entity
 *
 * @property int $id
 * @property string $mac
 * @property int $at_time
 * @property int $distance
 * @property int $event_id
 * @property int $sensor_id
 * @property int $hall_id
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\Event $event
 * @property \App\Model\Entity\Sensor $sensor
 * @property \App\Model\Entity\Hall $hall
 */
class Point extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'mac' => true,
        'at_time' => true,
        'distance' => true,
        'event_id' => true,
        'sensor_id' => true,
        'hall_id' => true,
        'created' => true,
        'event' => true,
        'sensor' => true,
        'hall' => true,
    ];
}
