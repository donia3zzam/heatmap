<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Hall Entity
 *
 * @property int $id
 * @property string $name
 * @property int $event_id
 * @property string $floor_image
 * @property int $height
 * @property int $width
 * @property int $number_of_sensors
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Event $event
 * @property \App\Model\Entity\Point[] $points
 * @property \App\Model\Entity\Sensor[] $sensors
 */
class Hall extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'event_id' => true,
        'floor_image' => true,
        'height' => true,
        'width' => true,
        'number_of_sensors' => true,
        'created' => true,
        'modified' => true,
        'event' => true,
        'points' => true,
        'sensors' => true,
    ];
}
