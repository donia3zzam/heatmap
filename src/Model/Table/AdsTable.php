<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Ads Model
 *
 * @property \App\Model\Table\CategoriesTable&\Cake\ORM\Association\BelongsTo $Categories
 * @property \App\Model\Table\LocationsTable&\Cake\ORM\Association\BelongsTo $Locations
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\FavoritesTable&\Cake\ORM\Association\HasMany $Favorites
 * @property \App\Model\Table\FieldsTable&\Cake\ORM\Association\BelongsToMany $Fields
 *
 * @method \App\Model\Entity\Ad newEmptyEntity()
 * @method \App\Model\Entity\Ad newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Ad[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Ad get($primaryKey, $options = [])
 * @method \App\Model\Entity\Ad findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Ad patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Ad[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Ad|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ad saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ad[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Ad[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Ad[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Ad[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AdsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('ads');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Locations', [
            'foreignKey' => 'location_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Favorites', [
            'foreignKey' => 'ad_id',
        ]);
        $this->belongsToMany('Fields', [
            'foreignKey' => 'ad_id',
            'targetForeignKey' => 'field_id',
            'joinTable' => 'ads_fields',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

//        $validator
//            ->scalar('slug')
//            ->maxLength('slug', 255)
//            ->requirePresence('slug', 'create')
//            ->notEmptyString('slug');

//        $validator
//            ->scalar('description')
//            ->requirePresence('description', 'create')
//            ->notEmptyString('description');

//        $validator
//            ->scalar('image_folder')
//            ->maxLength('image_folder', 255)
//            ->requirePresence('image_folder', 'create')
//            ->notEmptyFile('image_folder');
//
//        $validator
//            ->boolean('active')
//            ->requirePresence('active', 'create')
//            ->notEmptyString('active');



//        $validator
//            ->scalar('type')
//            ->maxLength('type', 255)
//            ->requirePresence('type', 'create')
//            ->notEmptyString('type');

//        $validator
//            ->integer('active_fo_days')
//            ->requirePresence('active_fo_days', 'create')
//            ->notEmptyString('active_fo_days');
//
//        $validator
//            ->boolean('featured')
//            ->requirePresence('featured', 'create')
//            ->notEmptyString('featured');
//
//        $validator
//            ->integer('views')
//            ->requirePresence('views', 'create')
//            ->notEmptyString('views');
//
//        $validator
//            ->integer('rate')
//            ->requirePresence('rate', 'create')
//            ->notEmptyString('rate');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['category_id'], 'Categories'));
        $rules->add($rules->existsIn(['location_id'], 'Locations'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
