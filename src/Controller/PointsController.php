<?php
declare(strict_types=1);

namespace App\Controller;


//use Tuupola\Trilateration\Intersection;
//use Tuupola;
use App\Tril\Point;
use App\Tril\Trilateration;
use Cake\ORM\TableRegistry;

/**
 * Points Controller
 *
 * @property \App\Model\Table\PointsTable $Points
 *
 * @method \App\Model\Entity\Point[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PointsController extends AppController
{
    var $hall_id;
    var $grid_data;
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Events', 'Sensors', 'Halls'],
        ];
        $points = $this->paginate($this->Points);

        $this->set(compact('points'));
    }


    function heatmap($hallId ){
        $this->Points->get_all_sensors_for_hall($hallId);
        $this->Points->get_all_uniq_macs();
        $this->Points->get_hall_average_spent_time();
        $start_end_point = $this->Points->get_start_end_timeStamps() ;


        $hallData = $this->Points->Halls->get($hallId);
        $start_datetime = date('Y-m-d',$start_end_point['first']);
        $end_datetime = date('Y-m-d',$start_end_point['last']);

        $this->set(compact('start_datetime','end_datetime','hallData'));
    }
    function eventManager($hallId=null){
        $userTable = TableRegistry::getTableLocator()->get('Users');
        $userData = $userTable->get($_SESSION['Auth']['User']['id']);
        $event = $this->Points->Events->get($userData->event_id,['contain'=>['Halls']]);
        $this->set('event',$event);
        if($hallId != null){
            $this->Points->get_all_sensors_for_hall($hallId);
            $unieqMacs = $this->Points->get_all_uniq_macs();
            $avrageSpentTime = $this->Points->get_hall_average_spent_time();
            $start_end_point = $this->Points->get_start_end_timeStamps() ;
            $hallData = $this->Points->Halls->get($hallId);
            $start_datetime = date('Y-m-d',$start_end_point['first']);
            $end_datetime = date('Y-m-d',$start_end_point['last']);

            $this->set(compact('start_datetime','end_datetime','hallData','unieqMacs','avrageSpentTime'));
        }
//        pr($event);

    }
    function cluster($hallId ){
        $this->Points->get_all_sensors_for_hall($hallId);
//        $this->Points->get_all_uniq_macs();
//        $this->Points->get_hall_average_spent_time();
        $start_end_point = $this->Points->get_start_end_timeStamps() ;
        $hallData = $this->Points->Halls->get($hallId);
        $start_datetime = date('Y-m-d',$start_end_point['first']);
        $end_datetime = date('Y-m-d',$start_end_point['last']);

        $this->set(compact('start_datetime','end_datetime','hallData'));
    }

    function newheat($hallId ){
        $this->Points->get_all_sensors_for_hall($hallId);
        $this->Points->get_all_uniq_macs();
        $this->Points->get_hall_average_spent_time();
        $start_end_point = $this->Points->get_start_end_timeStamps() ;
        $hallData = $this->Points->Halls->get($hallId);
        $start_datetime = date('Y-m-d',$start_end_point['first']);
        $end_datetime = date('Y-m-d',$start_end_point['last']);

        $this->set(compact('start_datetime','end_datetime','hallData'));
    }
    function getLocation($mac){
//        $this->Points->
    }



    function generate_grid($maxX,$maxY,$splitter){

        $counterX = 0;
        $return=[];
        for ($x=0;$x<=$maxX;$x+=$splitter){
            for ($y=0;$y<=$maxY;$y+=$splitter){
                $return[]=[
                    'startX' => $x,
                    'endX' => $x+$splitter,
                    'startY'=> $y,
                    'endY'=> $y+$splitter,
                    'centerX'   =>($x+$splitter) + ($splitter/2),
                    'centerY'   =>($y+$splitter) - ($splitter/2),
                    'data'=>[]
                ];
            }
        }

        return $return;
    }
    function test ($sensor)
    {
        error_reporting(-1);
        $this->disableAutoRender();
        $this->get_full_average_time(8);
//        var_dump($this->generate_grid(13600,6000,150));

    }

    function heatmapByTime ($hall,$time,$max,$min=0 , $algo=0 ,$des=0,$ignore=70, $base_rssi = 60 , $env_factor = 25 , $pallet = 25 , $mac=null){
        error_reporting(-1);
        $this->disableAutoRender();
        $this->hall_id = $hall;
        $this->Points->get_all_sensors_for_hall($hall);
        $unique = $this->Points->get_all_uniq_macs($time,$max,$min);
        $average = $this->Points->get_hall_average_spent_time();
        $mac_handeled = $this->handel_mac_array($unique,$mac);
//        var_dump($mac_handeled['ec:1f:72:a7:0d:2e']);
//        die();



        $returnData = [];
        $counter = 0;
        foreach ($mac_handeled as $mac => $data){
            $lastMin = 0;
            foreach ($data as $time_min => $time){
                $target_min = strtotime($time_min);

                if($lastMin == 0){
                    // add first point
                    if(count($time) > 2 ){
                        $a=new Trilateration();
                        $a->method = $a->methods[$des];
                        $a->ignore = $ignore;
                        $a->envirmentFactor = $env_factor;
                        $a->baseRSSI = $base_rssi;
                        $points = [];
                        foreach ($time as $point){
                            $points[] = $point;
                        }
                        $exactLocation =$a->Compute($points[0],$points[1],$points[2]);
                        $returnData[] = $exactLocation;
                        if($exactLocation['x'] > 0 && $exactLocation['y'] > 0
                            && $exactLocation['x'] < 13600 && $exactLocation['y'] < 6000
                        ){
                            $counter++;
                        }
                    }
                    $lastMin = $target_min;
                }else{
                    // add the point if its higher then the count of last + the $min*60
                    $lastMin = $lastMin + ($min*60) ;
                    if($target_min > $lastMin ){
                        if(count($time) > 2 ){
                            $a=new Trilateration();
                            $a->method = $a->methods[$des];
                            $a->ignore = $ignore;
                            $a->envirmentFactor = $env_factor;
                            $a->baseRSSI = $base_rssi;
                            $points = [];
                            foreach ($time as $point){
                                $points[] = $point;
                            }
                            $exactLocation =$a->Compute($points[0],$points[1],$points[2]);
                            $returnData[] = $exactLocation;
                            if($exactLocation['x'] > 0 && $exactLocation['y'] > 0
                                && $exactLocation['x'] < 13600 && $exactLocation['y'] < 6000
                            ){
                                $counter++;
                            }
                        }
                    }
                }



            }
        }
        if($algo==2){
            $pallet = $pallet * 10 ;
            $this->grid_data = $this->generate_grid(13600,6000,$pallet);
            foreach ($returnData as $key => $row){
                $varX = $returnData[$key]['x'];
                $varY = $returnData[$key]['y'];
                    $returnData[$key]['x'] =  (floor( ($varX / $pallet) ) *$pallet)-($pallet/2);
                    $returnData[$key]['y'] =  (floor( ($varY / $pallet) ) *$pallet)-($pallet/2);
            }
        }
        // $full_avrg = $this->get_full_average_time($hall);
        echo json_encode(['data' => $returnData,'uniquex'=>count($unique),'average'=>floor($average / 60 ),'full_average'=>floor($full_avrg / 60 )]);
        // echo json_encode(['data' => $returnData,'uniquex'=>count($unique),'average'=>floor($average / 60 ),'full_average'=>0]);





    }

    private function get_full_average_time($hall){
        $hall = $this->Points->Halls->get($hall,['contain' => ['Events']]);
        $time = strtotime($hall->event->event_start_day->toDateTimeString());
        $max  = strtotime($hall->event->event_end_day->toDateTimeString());
        $this->Points->get_all_sensors_for_hall($hall->id);
        $unique = $this->Points->get_all_uniq_macs($time,$max,1);
        $average = $this->Points->get_hall_average_spent_time();
        return $average;
    }



    function pins ($hall,$time,$max,$min=0 , $algo=0 ,$des=0,$ignore=70, $base_rssi = 60 , $env_factor = 25 , $pallet = 25 , $mac=null){
//        error_reporting(-1);
//        $this->disableAutoRender();
        $this->hall_id = $hall;
        $this->Points->get_all_sensors_for_hall($hall);
        $unique = $this->Points->get_all_uniq_macs($time,$max,$min);
        $average = $this->Points->get_hall_average_spent_time();
        $mac_handeled = $this->handel_mac_array($unique,$mac);
    //    var_dump($mac_handeled['ac:5f:3e:f4:e0:3f']);
//        die();

        $returnData = [];
        $counter = 0;
        foreach ($mac_handeled as $mac => $data){
            $lastMin = 0;
            foreach ($data as $time_min => $time){
                $target_min = strtotime($time_min);

                if($lastMin == 0){
                    // add first point
                    if(count($time) > 2 ){
                        $a=new Trilateration();
                        $a->method = $a->methods[$des];
                        $a->ignore = $ignore;
                        $a->envirmentFactor = $env_factor;
                        $a->baseRSSI = $base_rssi;
                        $points = [];
                        foreach ($time as $point){
                            $points[] = $point;
                        }
                        $exactLocation =$a->Compute($points[0],$points[1],$points[2]);
                        $exactLocation['time'] = $time_min;
                        $returnData[] = $exactLocation;
                        if($exactLocation['x'] > 0 && $exactLocation['y'] > 0
                            && $exactLocation['x'] < 13600 && $exactLocation['y'] < 6000
                        ){
                            $counter++;
                        }
                    }
                    $lastMin = $target_min;
                }else{
                    // add the point if its higher then the count of last + the $min*60
                    $lastMin = $lastMin + ($min*60) ;
                    if($target_min > $lastMin ){
                        if(count($time) > 2 ){
                            $a=new Trilateration();
                            $a->method = $a->methods[$des];
                            $a->ignore = $ignore;
                            $a->envirmentFactor = $env_factor;
                            $a->baseRSSI = $base_rssi;
                            $points = [];
                            foreach ($time as $point){
                                $points[] = $point;
                            }
                            $exactLocation =$a->Compute($points[0],$points[1],$points[2]);
                            $exactLocation['time'] = $time_min;
                            $returnData[] = $exactLocation;
                            if($exactLocation['x'] > 0 && $exactLocation['y'] > 0
                                && $exactLocation['x'] < 13600 && $exactLocation['y'] < 6000
                            ){
                                $counter++;
                            }
                        }
                    }
                }



            }
        }
    
//        echo json_encode(['data' => $returnData,'uniquex'=>count($unique),'average'=>floor($average / 60 )]);
//        var_dump($returnData);
//        die();
        $this->set('data' , $returnData);
        $hall = $this->Points->Halls->get($hall);

        $this->set(compact('hall'));




    }

    function fix_the_numbers($point){
        /*
         *  $return[]=[
                    'startX' => $x,
                    'endX' => $x+$splitter,
                    'startY'=> $y,
                    'endY'=> $y+$splitter,
                    'centerX'   =>($x+$splitter) + ($splitter/2),
                    'centerY'   =>($y+$splitter) - ($splitter/2),
                    'data'=>[]
                ];
         */
        foreach($this->grid_data as $cell){
            if($point['x'] >=$cell['startX'] &&
                $point['x'] <= $cell['endX'] &&
                $point['y'] >= $cell['startY'] &&
                $point['y'] <= $cell['endY']
            ){
//                var_dump($point);
//                var_dump($cell);
//                die();
                return [
                    'x'=>$cell['centerX'],
                    'y'=>$cell['centerY'],
                ];
            } else {
                return [
                    'x'=>$point['x'],
                    'y'=>$point['y'],
                ];
            }
        }
    }




    function heatmapByTimeVisitors ($hall,$time,$max,$min=0 , $mac=null ){
        error_reporting(-1);
        $this->disableAutoRender();
        $this->hall_id = $hall;
        $this->Points->get_all_sensors_for_hall($hall);
        $unique = $this->Points->get_all_uniq_macs($time,$max,$min);
        $average = $this->Points->get_hall_average_spent_time();
        $mac_handeled = $this->handel_mac_array($unique,$mac);
//        var_dump($mac_handeled['e8:3a:12:83:d5:29']);
//        die();

        $returnData = [];
        $counter = 0;
        foreach ($mac_handeled as $mac => $data){
            $lastMin = 0;
            foreach ($data as $time_min => $time){
//                $target_min = strtotime($time_min);
//                echo $target_min;
//                die();
//                $target_min = $target_min;

//                if( ($target_min +($min*60) ) > $lastMin){
                if(count($time) > 2 ){
                    $a=new Trilateration();
                    $points = [];
                    foreach ($time as $point){
                        $points[] = $point;
                    }
                    $exactLocation =$a->Compute($points[0],$points[1],$points[2]);
                    $returnData[] = $exactLocation;
                    if($exactLocation['x'] > 0 && $exactLocation['y'] > 0
                        && $exactLocation['x'] < 13600 && $exactLocation['y'] < 6000
                    ){
                        $counter++;
                    }
//                        $lastMin = $target_min+($min*60);
                }

//                }

            }
        }
        echo json_encode(['data' => $returnData,'uniquex'=>count($unique),'average'=>floor($average / 60 )]);





    }

    private function fixedPoints ($data){

    }

    function handel_mac_array($data,$targetMacAddress=null){
        /*
         * return Array [Mac][
         *  Time => [
         *      Sensor => ReadingArray[R]
         * ]
         *
         * ]
         */

        $returner = [];
        $sensors = $this->Points->Sensors->find('all')->where(['Sensors.hall_id'=>$this->hall_id])->toArray();

        $fixed_sensor = [];
        foreach ($sensors as $sensor) {
            $fixed_sensor[$sensor->id] = $sensor;
        }
        foreach ($data as $mac => $row){
            foreach ($row as $read){
                if (isset($read['0'])){
                    $time = date('d/m/Y H:i',(int)$read['0']);
                    if($targetMacAddress === null){
                        $returner[$mac][$time][$read['sensor']] = [
                            'sensor_x'  => $fixed_sensor[$read['sensor']]->location_x,
                            'sensor_y'  => $fixed_sensor[$read['sensor']]->location_y,
                            'r' => $read[2],
                            'name' => $fixed_sensor[$read['sensor']]->name
                        ];
                    }
                    else{

                        if($mac == $targetMacAddress){
                            $returner[$mac][$time][$read['sensor']] = [
                                'sensor_x'  => $fixed_sensor[$read['sensor']]->location_x,
                                'sensor_y'  => $fixed_sensor[$read['sensor']]->location_y,
                                'r' => $read[2],
                                'name' => $fixed_sensor[$read['sensor']]->name
                            ];
                        }
                    }
                }
            }

//            $target_sensor = $row['sensor'];
//            $returner[$mac] = [
//                "r" =>$row[2],
//                "x" => $fixed_sensor[$target_sensor]->location_x,
//                "y" =>$fixed_sensor[$target_sensor]->location_y,
//            ];
            if(is_array($mac)){
                ksort($returner[$mac]);
            }
        }
        return $returner;
    }


    function get_start_end_timeStamps($hall){
        $finder = $this->Points->find('all')->order(['at_time'])->select(['at_time'])->where(['hall_id'=>$hall])->all();
        $first = $finder->first();
        $last = $finder->last();

        return [
            'first'=>$first->at_time,
            'last'=>$last->at_time,
            ];
    }

    /**
     * View method
     *
     * @param string|null $id Point id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $point = $this->Points->get($id, [
            'contain' => ['Events', 'Sensors', 'Halls'],
        ]);

        $this->set('point', $point);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $point = $this->Points->newEmptyEntity();
        if ($this->request->is('post')) {
            $point = $this->Points->patchEntity($point, $this->request->getData());
            if ($this->Points->save($point)) {
                $this->Flash->success(__('The point has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The point could not be saved. Please, try again.'));
        }
        $events = $this->Points->Events->find('list', ['limit' => 200]);
        $sensors = $this->Points->Sensors->find('list', ['limit' => 200]);
        $halls = $this->Points->Halls->find('list', ['limit' => 200]);
        $this->set(compact('point', 'events', 'sensors', 'halls'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Point id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $point = $this->Points->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $point = $this->Points->patchEntity($point, $this->request->getData());
            if ($this->Points->save($point)) {
                $this->Flash->success(__('The point has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The point could not be saved. Please, try again.'));
        }
        $events = $this->Points->Events->find('list', ['limit' => 200]);
        $sensors = $this->Points->Sensors->find('list', ['limit' => 200]);
        $halls = $this->Points->Halls->find('list', ['limit' => 200]);
        $this->set(compact('point', 'events', 'sensors', 'halls'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Point id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $point = $this->Points->get($id);
        if ($this->Points->delete($point)) {
            $this->Flash->success(__('The point has been deleted.'));
        } else {
            $this->Flash->error(__('The point could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
//
//class MPoint {
//    private $lt;private $ln;private $r;
//    public function __construct($lt,$ln,$r){$this->lt=$lt;$this->ln=$ln;$this->r=$r;}
//    public function glt(){return $this->lt;}
//    public function gln(){return $this->ln;}
//    public function gr(){return $this->r;}
//}
