<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

/**
 * Sensors Controller
 *
 * @property \App\Model\Table\SensorsTable $Sensors
 *
 * @method \App\Model\Entity\Sensor[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SensorsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Events', 'Halls'],
        ];
        $sensors = $this->paginate($this->Sensors);

        $this->set(compact('sensors'));
    }

    /**
     * View method
     *
     * @param string|null $id Sensor id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $sensor = $this->Sensors->get($id, [
            'contain' => ['Events', 'Halls', 'Points'],
        ]);

        $this->set('sensor', $sensor);
    }

    public function gpoints($id = null,$try=0)
    {
        $limit = 10000;
        $startPoint = $limit*$try;
        $this->disableAutoRender();
        $sensor = $this->Sensors->get($id, [
            'contain' => ['Events', 'Halls', 'Points'],
        ]);
        $filePath = ROOT.DS.'webroot'.DS.'uploads'.DS.$id.DS.'data.txt';
        $file = new File($filePath);
        $contents = $file->read();
        $arrayOfPoints = explode('
',$contents);
        $count = 0;
        $entities = [];
        //limitations =
        $nextRow = $limit + $startPoint +1;
        $nextPage = isset($arrayOfPoints[$nextRow])?true:false;
        $arrayOfPoints = array_slice($arrayOfPoints,$startPoint,$limit);

        foreach ($arrayOfPoints as $point){
            if($point != ''){
                $point =explode('|',$point);
                $entity = $this->Sensors->Points->newEmptyEntity();
                $entity->mac = trim($point[1]);
                $entity->at_time =trim($point[0]);
                $entity->distance = trim($point[2]);
                $entity->event_id = $sensor->event_id;
                $entity->sensor_id = $sensor->id;
                $entity->hall_id = $sensor->hall_id;
                $entities[]=$entity;
//                $this->Sensors->Points->save($entity);
                $count++;
            }


        }

        $this->Sensors->Points->saveMany($entities);
        if($nextRow){
            return $this->redirect(['action' => 'gpoints',$id,$try+1]);
        }
        $this->Flash->success(__('The Point has been saved. '.$count.' Number of points '));

        return $this->redirect(['action' => 'index']);


        $this->set('sensor', $sensor);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $sensor = $this->Sensors->newEmptyEntity();
        if ($this->request->is('post')) {
            $sensor = $this->Sensors->patchEntity($sensor, $this->request->getData());
            if ($this->Sensors->save($sensor)) {
                $this->Flash->success(__('The sensor has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sensor could not be saved. Please, try again.'));
        }
        $events = $this->Sensors->Events->find('list', ['limit' => 200]);
        $halls = $this->Sensors->Halls->find('list', ['limit' => 200]);
        $this->set(compact('sensor', 'events', 'halls'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Sensor id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sensor = $this->Sensors->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sensor = $this->Sensors->patchEntity($sensor, $this->request->getData());
            if ($this->Sensors->save($sensor)) {
                $this->Flash->success(__('The sensor has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sensor could not be saved. Please, try again.'));
        }
        $events = $this->Sensors->Events->find('list', ['limit' => 200]);
        $halls = $this->Sensors->Halls->find('list', ['limit' => 200]);
        $this->set(compact('sensor', 'events', 'halls'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Sensor id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $sensor = $this->Sensors->get($id);
        if ($this->Sensors->delete($sensor)) {
            $this->Flash->success(__('The sensor has been deleted.'));
        } else {
            $this->Flash->error(__('The sensor could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
