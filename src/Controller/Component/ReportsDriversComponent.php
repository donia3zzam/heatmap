<?php
declare(strict_types=1);

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;

/**
 * ReportsDrivers component
 */
class ReportsDriversComponent extends ReportsComponent
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    var $driversModel;
    public function __construct(ComponentRegistry $registry, array $config = [])
    {
        parent::__construct($registry, $config);
        $this->setTheTargetModel('DriversOrders') ;
        $this->driversModel = TableRegistry::getTableLocator()->get('Drivers');

    }

    function get_all_orders_count_per_user($userID){
        return $this->get_count(['user_id' =>$userID]);
    }
    function get_all_on_route_orders(){
        $this->setTheTargetModel('Orders') ;

        return $this->get_count(['statues' =>'On route']);
    }
    function get_count_of_diliveries(){
        $this->setTheTargetModel('DriversOrders') ;
        return $this->get_count([]);

    }
    function get_count_of_diliveries_last_days($days){
        $this->setTheTargetModel('DriversOrders') ;
        return $this->get_count(['target_day > ' => $this->getDate($days)]);
    }




    function get_active_drivers($days = 30){
        // return the drivers ids count the last 30 days
        $this->setTheTargetModel('DriversOrders') ;

        $query = $this->model->find();
        $res = $query->select(['driver_id' , 'total' => 'count(driver_id)'])
            ->where(['created >' => $this->getDate($days) ])
            ->group(['driver_id'])->order(['total'=>'DESC'])->map(function ($row) {
            return[
                'driver_id' =>$row->driver_id,
                'total' =>$row->total,
                'driver' =>$this->driversModel->get($row->driver_id),
            ];
        })->toArray();
       return $res;
    }
    function most_active_drivers(){
        // return a list of dataTable of most achievable drivers
        $this->get_active_drivers(365);
    }
    function get_active_drivers_last_month_count (){
        return count($this->get_active_drivers());
    }
    function get_full_driver_data($driverId ){
        $conditions = [ ];
        $this->setTheTargetModel('DriversOrders') ;
        return [
            'total_orders_count' => $this->get_driver_orders_count($driverId),
            'total_active_orders_count' => $this->get_driver_active_count($driverId),
            'total_active_orders_sum' => $this->get_driver_active_sum($driverId),
            'total_driver_orders_sum' => $this->get_driver_all_sum($driverId),
        ];
    }
    function get_driver_orders_count($driverId){
        $this->setTheTargetModel('DriversOrders') ;
        return $this->get_count(['driver_id'=>$driverId]);
    }
    function get_driver_active_count($driverId){
        $this->setTheTargetModel('DriversOrders') ;
        return $this->get_count(['DriversOrders.driver_id'=>$driverId,'Orders.statues'=>'On route'],['Orders']);
    }
    function get_driver_active_sum($driverId){
        $this->setTheTargetModel('DriversOrders') ;
        return $this->model->find()->contain('Orders') ->
        where(['DriversOrders.driver_id'=>$driverId,'Orders.statues'=>'On route'])->select(['total_cod'=>'sum(Orders.cod)'])->toArray()[0]->total_cod  ;

    }
    public function get_statues_counts($driverId){
        $this->setTheTargetModel('DriversOrders') ;
        $results = $this->model->find()
            ->contain('Orders')->where(['DriversOrders.driver_id'=>$driverId])->select(['statues' => 'Orders.statues',
                'total'=>'count(Orders.statues)',
                ])
            ->group(['Orders.statues'])->order(['total'=>'DESC'])->map(function($row){
                return [
                    'statues' => $row['statues'],
                    'total' => $row['total']

                    ];
            })->toArray();
        return $results;
    }
    function get_driver_all_sum($driverId){
        $this->setTheTargetModel('DriversOrders') ;
        return $this->model->find()->contain('Orders') ->
        where(['DriversOrders.driver_id'=>$driverId])->select(['total_cod'=>'sum(Orders.cod)'])->toArray()[0]->total_cod  ;

    }



    function get_orders_timeline_by_driver($driverId ){
        $this->setTheTargetModel('DriversOrders') ;

        $conditions = ['driver_id' => $driverId];
        $duration = 7;
        $all = [

            'count'   =>$this->get_timeline_data('order_id' , $conditions,'count',null, $duration),
        ];
        $enhanced_return =[];
        foreach ($all['count'] as $date => $value){
            $enhanced_return[$date] = [
                'count'     =>$all['count'][$date],
            ];
        }
        return $enhanced_return ;
    }

    /**
     * @param $days
     * @return false|string
     */
    public function getDate($days)
    {
        return date('Y-m-d', (time() - (86400 * $days)));
    }
}
