<?php
declare(strict_types=1);

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

/**
 * ReportsOrders component
 */
class ReportsOrdersComponent extends ReportsComponent
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    public function __construct(ComponentRegistry $registry, array $config = [])
    {
        parent::__construct($registry, $config);
        $this->setTheTargetModel('Orders') ;
    }

    function get_all_orders_count_per_user($userID){
        return $this->get_count(['user_id' =>$userID]);
    }
    public function get_due_orders_count(){
        return $this->get_count(['Orders.statues IN' => state_active_type , 'created < ' => date('Y-m-d', (time() - 259200 ) ) ] );
    }
    public function last_24_hours_orders(){
        return $this->get_count(['Orders.statues IN' => state_active_type , 'created > ' => date('Y-m-d', (time() - 86400 ) ) ] );
    }
    public function get_statues_counts($conds = [],$limit = 4){
        return $this->get_values_of_feild('statues',$conds,$limit);
    }
    function get_full_orders_data($userId =null){
        $conditions = ['Orders.statues IN' => state_active_type ];
        if($userId != null){
            $conditions ['user_id'] = $userId;
        }
        return [
            'total_orders_count' => $this->get_count($conditions),
            'total_orders_amount_sum' => $this->get_sum_of_field('cod' , $conditions),
            'total_orders_amount_avg' => $this->get_avg_of_field('cod' , $conditions),
            'total_orders_fees_sum'   => $this->get_sum_of_field('fees' , $conditions)+500,
            'total_orders_fees_avg'   => $this->get_avg_of_field('fees' , $conditions),
        ];
    }

    function get_all_full_orders_data($userId =null){
        $conditions = [ ];
        if($userId != null){
            $conditions ['user_id'] = $userId;
        }
        return [
            'total_orders_count' => $this->get_count($conditions),
            'total_orders_amount_sum' => $this->get_sum_of_field('cod' , $conditions),
            'total_orders_amount_avg' => $this->get_avg_of_field('cod' , $conditions),
            'total_orders_fees_sum'   => $this->get_sum_of_field('fees' , $conditions)+500,
            'total_orders_fees_avg'   => $this->get_avg_of_field('fees' , $conditions),
        ];
    }

    function get_orders_timeline_per_user($userId = 0 ){
        $conditions = $userId == 0 ? [] : ['user_id' => $userId];
        $duration = 30;
        $all = [
            'sum'   =>$this->get_timeline_data('cod' , $conditions,'sum',null, $duration),
            'avg'   =>$this->get_timeline_data('cod' , $conditions,'avg',null, $duration),
            'count'   =>$this->get_timeline_data('cod' , $conditions,'count',null, $duration),
        ];
        $enhanced_return =[];
        foreach ($all['sum'] as $date => $value){
            $enhanced_return[$date] = [
                'sum'       =>$value,
                'avg'       =>$all['avg'][$date],
                'count'     =>$all['count'][$date],
            ];
        }
        return $enhanced_return ;
    }

}
