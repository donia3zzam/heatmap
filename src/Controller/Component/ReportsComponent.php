<?php
declare(strict_types=1);

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

/**
 * Reports component
 */
class ReportsComponent extends Component
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    protected $model;
    function setTheTargetModel ($model){
        $this->model = TableRegistry::getTableLocator()->get($model);
    }

    function get_count(array $conditions,$secondModel = null){
        if($secondModel == null)
            $secondModel = [];
        return $this->model->find()->contain($secondModel)->where($conditions)->count();
    }
    function get_sum_of_field($field,$conditions,$secondModel = null){
        if($secondModel == null)
            $secondModel = [];
        return $this->model->find()->contain($secondModel)->where($conditions)->sumOf($field);
    }

    function get_avg_of_field($field,$conditions){
        $query = $this->model->find();
        $query->select(['avg_'.$field => $query->func()->avg($field)])
            ->where($conditions)
            ->first();
        return $query->toArray()['0']['avg_'.$field];
    }

    function get_timeline_data($total_of_field,$conditions,$type= 'sum',$start = null , $duration = 7,$timeField = 'created'){
        //get start day if not set
        $return=[];
        $first = $this->model->find()->order(['id'=>'asc'])->where($conditions)->first();

        if(!empty($first)) {
            if($start == null){
                $start = $first->$timeField;
            }
            $startTimestamp = $start->toUnixString();
            $currentTime = time();
            $splliter = (60 * 60 * 24) * $duration;
            for ($x = $startTimestamp; $x < $currentTime; $x += $splliter) {
                $conditions[$timeField . ' >'] = date('Y-m-d', (int)$x);
                $conditions[$timeField . ' <'] = date('Y-m-d', ((int)$x + $splliter));
                if ($type == 'count')
                    $return[date('d-M-Y', (int)$x)] = $this->get_count($conditions);
                if ($type == 'sum')
                    $return[date('d-M-Y', (int)$x)] = $this->get_sum_of_field($total_of_field, $conditions);
                if ($type == 'avg')
                    $return[date('d-M-Y', (int)$x)] = $this->get_avg_of_field($total_of_field, $conditions);

            }
            return $return;
        }
    }

    function get_values_of_feild ($field,$conditions=[] ,$limit = 10000){
        $connection = ConnectionManager::get('default');
        $lowerModelName = $this->model->getTable();
        $where = $this->conditions_to_where_statment($conditions);
        $results = $connection
            ->execute(
                "select $field, count($field) AS total , (COUNT(*) / (SELECT COUNT(*) FROM orders)) * 100 as percentage from $lowerModelName $where group by $field order by total desc LIMIT $limit"
                )
            ->fetchAll('assoc');
        return $results;
    }

    private function  conditions_to_where_statment($conditions){
        if(!empty($conditions)) {
            $return = "WHERE ";
            $nextIndex = 1;
            foreach ($conditions as $key => $value) {
                $return .= " $key = $value ";
                if(isset($conditions[$nextIndex]))
                    $return .="AND ";
                $nextIndex++;
            }
            return $return;
        }
    }

    function best_of($field,$conditions,$return_number =5,$contain=[] ){
       return $this->model->find('all')->where($conditions)->contain($contain)->order([$field => 'DESC'])->limit($return_number)->toArray();
    }

    function best_of_by_other_model($secondModel,$avg_field,$sum_field,$count_field = 'id',$order_by=count){
        $connection = ConnectionManager::get('default');
        $modelTable = $this->model->getTable();
        $results = $connection
            ->execute(
                "
            SELECT $modelTable.*, AVG($secondModel.cod) AS model_avg , SUM($secondModel.cod) AS model_sum,count($secondModel.user_id) AS model_count FROM $modelTable
            LEFT JOIN $secondModel ON $modelTable.id = $secondModel.user_id GROUP BY $modelTable.id ORDER BY model_".$order_by." DESC
 "
            )
            ->fetchAll('assoc');
        return $results;
    }

    /**
     * @param $days
     * @return false|string
     */
    public function getDate($days)
    {
        return date('Y-m-d', (time() - (86400 * $days)));
    }

}
