<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Ads Controller
 *
 * @property \App\Model\Table\AdsTable $Ads
 *
 * @method \App\Model\Entity\Ad[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AdsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Categories', 'Locations', 'Users'],
        ];
        $ads = $this->paginate($this->Ads);

        $this->set(compact('ads'));
    }

    public function homepage()
    {
        $this->viewBuilder()->setLayout('front/main');
        $this->paginate = [
            'contain' => ['Categories', 'Locations', 'Users'],
        ];
        $ads = $this->paginate($this->Ads);
        $categories = $this->Ads->Categories->find('all')->where(['parent_id IS'=>null])->contain(['ChildCategories'])->toArray();
        $this->set(compact('ads','categories'));
    }

    /**
     * View method
     *
     * @param string|null $id Ad id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ad = $this->Ads->get($id, [
            'contain' => ['Categories', 'Locations', 'Users', 'Fields', 'Favorites'],
        ]);

        $this->set('ad', $ad);
    }

    function create(){
        $this->set('hide_cats', true);
        $this->viewBuilder()->setLayout('front/main');
        $ad = $this->Ads->newEmptyEntity();
        if ($this->request->is('post')) {
            $fieldsList = $this->request->getData()['Fields'];
            $ad = $this->Ads->patchEntity($ad, $this->request->getData(),['associated' => ['Fields._joinData']]);

            if ($this->Ads->save($ad,['associated' => ['Fields._joinData']])) {
                $this->saveFields($fieldsList,$ad->id);
                $this->Flash->success(__('The ad has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ad could not be saved. Please, try again.'));
        }
        $categories = $this->Ads->Categories->find('list', ['limit' => 200])->where(['parent_id IS' => null]);
        $subCategory = isset($_GET['cat_id']) ? $this->Ads->Categories->find('list', ['limit' => 200])->where(['parent_id' => $_GET['cat_id'] ]):[];
        $locations = $this->Ads->Locations->find('list', ['limit' => 200]);
        $users = $this->Ads->Users->find('list', ['limit' => 200]);
        $fields = $this->Ads->Fields->find('list', ['limit' => 200]);
        $this->set(compact('ad', 'categories', 'locations', 'users', 'fields','subCategory'));
    }

    function listing($keyword = null){
        if(isset($_GET['keyword']))
            $keyword = $_GET['keyword'];
        $this->viewBuilder()->setLayout('front/main');
        $this->set('hide_cats', true);
        $conditions = [];
        if($keyword != null)
            $conditions['OR'] = [
                'Ads.name LIkE' => '%'.$keyword.'%',
                'Ads.description LIkE' => '%'.$keyword.'%',
            ];
        $this->set('ads',$this->Ads->find('all')->where($conditions)->contain(['Users','Categories','Locations']));
    }
    public function details($id = null)
    {
        $this->viewBuilder()->setLayout('front/main');

        $ad = $this->Ads->get($id, [
            'contain' => ['Categories', 'Locations', 'Users', 'Fields', 'Favorites'],
        ]);
        $recent_ads = $this->Ads->find('all')->order(['id'=>'DESC'])->limit(4);

        $this->set('ad', $ad);
        $this->set('recent_ads', $recent_ads);
        $this->set('hide_cats', true);
        $this->set('main_categories', $this->Ads->Categories->find('all')->where(['parent_id IS'=>null]));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {

        $ad = $this->Ads->newEmptyEntity();
        if ($this->request->is('post')) {

//            $ad = $this->Ads->patchEntity($ad, $this->request->getData());
            $fieldsList = $this->request->getData()['Fields'];
            $ad = $this->Ads->patchEntity($ad, $this->request->getData(),['associated' => ['Fields._joinData']]);

            if ($this->Ads->save($ad,['associated' => ['Fields._joinData']])) {
                $this->saveFields($fieldsList,$ad->id);
                $this->Flash->success(__('The ad has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ad could not be saved. Please, try again.'));
        }
        $categories = $this->Ads->Categories->find('list', ['limit' => 200])->where(['parent_id IS' => null]);
        $subCategory = isset($_GET['cat_id']) ? $this->Ads->Categories->find('list', ['limit' => 200])->where(['parent_id' => $_GET['cat_id'] ]):[];
        $locations = $this->Ads->Locations->find('list', ['limit' => 200]);
        $users = $this->Ads->Users->find('list', ['limit' => 200]);
        $fields = $this->Ads->Fields->find('list', ['limit' => 200]);
        $this->set(compact('ad', 'categories', 'locations', 'users', 'fields','subCategory'));
    }

    function saveFields($fieldList,$id){
        $this->loadModel('AdsFields');
        $entries = [];
        foreach ($fieldList as $item) {
            $entity  = $this->AdsFields->newEmptyEntity();
            $entity->ad_id = $id;
            $entity->field_id = $item['id'];
            $entity->value = $item['_joinData']['value'];
            $entries[] = $entity;
        }
        $this->AdsFields->saveMany($entries);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ad id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ad = $this->Ads->get($id, [
            'contain' => ['Fields'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ad = $this->Ads->patchEntity($ad, $this->request->getData());
            if ($this->Ads->save($ad)) {
                $this->Flash->success(__('The ad has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ad could not be saved. Please, try again.'));
        }
        $categories = $this->Ads->Categories->find('list', ['limit' => 200]);
        $locations = $this->Ads->Locations->find('list', ['limit' => 200]);
        $users = $this->Ads->Users->find('list', ['limit' => 200]);
        $fields = $this->Ads->Fields->find('list', ['limit' => 200]);
        $this->set(compact('ad', 'categories', 'locations', 'users', 'fields'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Ad id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ad = $this->Ads->get($id);
        if ($this->Ads->delete($ad)) {
            $this->Flash->success(__('The ad has been deleted.'));
        } else {
            $this->Flash->error(__('The ad could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }


    function login (){
        $this->viewBuilder()->setLayout('front/main');
        $this->set('hide_cats', true);
        $this->set('hide_search', true);

    }
    function register (){
        $this->viewBuilder()->setLayout('front/main');
        $this->set('hide_cats', true);
        $this->set('hide_search', true);

    }
}
