<?php
declare(strict_types=1);
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Notifictions\Event\NotificationListener;

$hooks = \App\Hooks\Hooks::getInstance();

$hooks->add_action('action_test','testing_funciton_hello');
function testing_funciton_hello($test){
    echo "Hi : ".$test;
}
$hooks->add_action('Orders_List_afterTable','Orders_List_afterTable');
function Orders_List_afterTable(){
    $view =  new \App\View\AppView();

    echo $view->element('backend/datatablejs');
}

$hooks->add_verify('system_show_data_table');

function example_callback( $example ) {
   if(1==2){
       return $example;
   } else
       return "";
}
$hooks->add_filter( 'example_filter', 'example_callback' );
//$hooks->remove_verify('system_show_data_table','system_show_data_table');

