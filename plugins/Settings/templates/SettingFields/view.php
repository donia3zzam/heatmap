<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $settingField
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('Edit Setting Field'), ['action' => 'edit', $settingField->id], ['class' => 'nav-link']) ?></li>
<li><?= $this->Form->postLink(__('Delete Setting Field'), ['action' => 'delete', $settingField->id], ['confirm' => __('Are you sure you want to delete # {0}?', $settingField->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Setting Fields'), ['action' => 'index'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('New Setting Field'), ['action' => 'add'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('List Setting Groups'), ['controller' => 'SettingGroups', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Setting Group'), ['controller' => 'SettingGroups', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="settingFields view large-9 medium-8 columns content">
    <h3><?= h($settingField->name) ?></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($settingField->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Type') ?></th>
                <td><?= h($settingField->type) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Setting Group') ?></th>
                <td><?= $settingField->has('setting_group') ? $this->Html->link($settingField->setting_group->name, ['controller' => 'SettingGroups', 'action' => 'view', $settingField->setting_group->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Placeholder') ?></th>
                <td><?= h($settingField->placeholder) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($settingField->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($settingField->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($settingField->modified) ?></td>
            </tr>
        </table>
    </div>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($settingField->description)); ?>
    </div>
    <div class="row">
        <h4><?= __('Stored Val') ?></h4>
        <?= $this->Text->autoParagraph(h($settingField->stored_val)); ?>
    </div>
    <div class="row">
        <h4><?= __('Help Text') ?></h4>
        <?= $this->Text->autoParagraph(h($settingField->help_text)); ?>
    </div>
</div>
