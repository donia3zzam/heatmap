<?php
//pr($users_full_data);
?>


<div class="row">


    <?php
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'yellow',
        'size' => 2,
        'count' => $users_full_data['all_users_count'],
        'title' => __('Users Count '),
        'icon' => 'truck',
        'sub_msg' => ' ',
    ]);
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'blue',
        'size' => 2,
        'count' => $users_full_data['all_users_active'],
        'title' => __('Active Users'),
        'icon' => 'truck',
        'sub_msg' => 'Users with orders recently ' ,
    ]);
    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'cyan',
        'size' => 2,
        'count' => $users_full_data['all_users_last_day'],
        'title' => __('Last day activities '),
        'icon' => 'file-invoice-dollar',
        'sub_msg' => 'Orders within 24 hours   ',
    ]);

    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'maroon',
        'size' => 2,
        'count' => $users_full_data['all_users_inactive'],
        'title' => __('Inactive users '),
        'icon' => 'file-invoice-dollar',
        'sub_msg' => 'None active last 7 days ',
    ]);

    echo $this->element('Utils.backend/widgets/count_block',[
        'color' => 'white',
        'size' => 2,
        'count' => $users_full_data['all_users_last_week'],
        'title' => __('Last Week Registrations '),
        'icon' => 'file-invoice-dollar',
        'sub_msg' => 'Accounts created ',
    ]);
//
//    echo $this->element('Utils.backend/widgets/count_block',[
//        'color' => 'dark',
//        'size' => 2,
//        'count' => $users_full_data['total_on_route_fees'],
//        'title' => __('Total on routes Fees '),
//        'icon' => 'file-invoice-dollar',
//        'sub_msg' => 'Fees Of on routes ',
//    ]);
//
//    echo $this->element('Utils.backend/widgets/count_block',[
//        'color' => 'light',
//        'size' => 2,
//        'count' => $users_full_data['total_active_orders_fees'],
//        'title' => __('Total UpComing Fees '),
//        'icon' => 'file-invoice-dollar',
//        'sub_msg' => 'Fees count for active ',
//    ]);
//
//    echo $this->element('Utils.backend/widgets/count_block',[
//        'color' => 'gray',
//        'size' => 2,
//        'count' => $users_full_data['total_due_orders_count'],
//        'title' => __('Due Orders '),
//        'icon' => 'file-invoice-dollar',
//        'sub_msg' => 'On hold orders 3 days ago ',
//    ]);
//
//    echo $this->element('Utils.backend/widgets/count_block',[
//        'color' => 'blue',
//        'size' => 2,
//        'count' => $users_full_data['total_due_orders_cods'],
//        'title' => __('Due Cods '),
//        'icon' => 'file-invoice-dollar',
//        'sub_msg' => 'COD for due orders 3 days ago  ',
//    ]);
//
//    echo $this->element('Utils.backend/widgets/count_block',[
//        'color' => 'red',
//        'size' => 2,
//        'count' => $users_full_data['total_due_orders_fees'],
//        'title' => __('Due Fees  '),
//        'icon' => 'file-invoice-dollar',
//        'sub_msg' => 'Fees for on hold orders 3 days ago  ',
//    ]);

    //
    ?>
</div>




<div class="row">

    <section class="col-lg-6 connectedSortable">
        <?php
        echo $this->element('Utils.backend/widgets/chart_dimminsions',[
            'canv_id' => 'chartscanv',
            'height' => '600',
            'title' => 'Users Since account creation  ',
            'icon' => 'file-invoice-dollar',
            'chart_type' => 'bar',
            'cahrtData' => $users_full_data['users_timeline'],
            'scale' =>'EGP'
        ]);

        ?>
    </section>


    <section class="col-lg-6 connectedSortable">
        <?php
        echo $this->element('Utils.backend/widgets/chart_dimminsions',[
            'canv_id' => 'chartscanv2',
            'height' => '600',
            'title' => 'Users Since account creation  ',
            'icon' => 'file-invoice-dollar',
            'chart_type' => 'pie',
            'cahrtData' => $users_full_data['users_timeline'],
            'scale' =>'EGP'
        ]);

        ?>
    </section>

</div>

<?php
echo $this->element('Utils.backend/widgets/data_table',[
    'color' => 'green',
    'size' => '12',
    'table_data' => $users_full_data['trade_levels'],
    'title' => __('Users Trade Levels'),
    'icon' => 'file-invoice-dollar',
    'sub_msg' => 'Order Status List ' ,

]);
?>
<?php
echo $this->element('Utils.backend/widgets/data_table',[
    'color' => 'blue',
    'size' => '12',
    'table_data' => $users_full_data['best_users_count'],
    'title' => __('Best Users List (full data)'),
    'icon' => 'file-invoice-dollar',
    'sub_msg' => 'Order Status List ' ,

]);
?>
