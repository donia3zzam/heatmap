<?php
//pr($orders_full_data);
?>


<div class="row">


            <?php
            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'yellow',
                'size' => 2,
                'count' => $orders_full_data['total_count'],
                'title' => __('Total Orders '),
                'icon' => 'truck',
                'sub_msg' => ' ',
            ]);
            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'blue',
                'size' => 2,
                'count' => $orders_full_data['total_inactive_count'],
                'title' => __('Inactive Orders'),
                'icon' => 'truck',
                'sub_msg' => 'Orders with Inactive statues' ,
            ]);
            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'cyan',
                'size' => 2,
                'count' => $orders_full_data['total_on_route_count'],
                'title' => __('On Routes '),
                'icon' => 'file-invoice-dollar',
                'sub_msg' => 'Orders with drivers  ',
            ]);

            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'maroon',
                'size' => 2,
                'count' => $orders_full_data['total_on_route_cods'],
                'title' => __('On Routes CODS '),
                'icon' => 'file-invoice-dollar',
                'sub_msg' => 'Total Count for cods',
            ]);

            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'white',
                'size' => 2,
                'count' => $orders_full_data['total_active_orders_cods'],
                'title' => __('Total UpComing CODS '),
                'icon' => 'file-invoice-dollar',
                'sub_msg' => 'Cods Count for active orders ',
            ]);

            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'dark',
                'size' => 2,
                'count' => $orders_full_data['total_on_route_fees'],
                'title' => __('Total on routes Fees '),
                'icon' => 'file-invoice-dollar',
                'sub_msg' => 'Fees Of on routes ',
            ]);

            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'light',
                'size' => 2,
                'count' => $orders_full_data['total_active_orders_fees'],
                'title' => __('Total UpComing Fees '),
                'icon' => 'file-invoice-dollar',
                'sub_msg' => 'Fees count for active ',
            ]);

            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'gray',
                'size' => 2,
                'count' => $orders_full_data['total_due_orders_count'],
                'title' => __('Due Orders '),
                'icon' => 'file-invoice-dollar',
                'sub_msg' => 'On hold orders 3 days ago ',
            ]);

            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'blue',
                'size' => 2,
                'count' => $orders_full_data['total_due_orders_cods'],
                'title' => __('Due Cods '),
                'icon' => 'file-invoice-dollar',
                'sub_msg' => 'COD for due orders 3 days ago  ',
            ]);

            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'red',
                'size' => 2,
                'count' => $orders_full_data['total_due_orders_fees'],
                'title' => __('Due Fees  '),
                'icon' => 'file-invoice-dollar',
                'sub_msg' => 'Fees for on hold orders 3 days ago  ',
            ]);

            //
            ?>
        </div>

<div class="row">

<section class="col-lg-6 connectedSortable">
    <?php
    echo $this->element('Utils.backend/widgets/chart_dimminsions',[
        'canv_id' => 'chartscanv',
        'height' => '600',
        'title' => 'Orders Since account creation  ',
        'icon' => 'file-invoice-dollar',
        'chart_type' => 'bar',
        'cahrtData' => $orders_full_data['total_orders_timeline'],
        'scale' =>'EGP'
    ]);

    ?>
</section>


<section class="col-lg-6 connectedSortable">
    <?php
    echo $this->element('Utils.backend/widgets/chart_dimminsions',[
        'canv_id' => 'chartscanv2',
        'height' => '600',
        'title' => 'Orders Since account creation  ',
        'icon' => 'file-invoice-dollar',
        'chart_type' => 'pie',
        'cahrtData' => $orders_full_data['total_orders_timeline'],
        'scale' =>'EGP'
    ]);

    ?>
</section>

</div>


<?php
echo $this->element('Utils.backend/widgets/data_table',[
    'color' => 'green',
    'size' => '12',
    'table_data' => $orders_full_data['total_orders_statues_table'],
    'title' => __('Order Statues '),
    'icon' => 'file-invoice-dollar',
    'sub_msg' => 'Order Status List ' ,

]);
?>
