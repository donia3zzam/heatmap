<?php
//pr($invoices_full_data);

?>

<div class="row">


            <?php
            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'yellow',
                'size' => 2,
                'count' => $invoices_full_data['total_count'],
                'title' => __('Total Invoices '),
                'icon' => 'truck',
                'sub_msg' => ' ',
            ]);
            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'blue',
                'size' => 2,
                'count' => $invoices_full_data['total_count_open'],
                'title' => __('Open Invoices'),
                'icon' => 'truck',
                'sub_msg' => 'Invoices not cleared yet' ,
            ]);
            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'cyan',
                'size' => 2,
                'count' => $invoices_full_data['total_orders_count_open'],
                'title' => __('Open invoices orders '),
                'icon' => 'file-invoice-dollar',
                'sub_msg' => 'Not paid  but collected  ',
            ]);

            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'maroon',
                'size' => 2,
                'count' => $invoices_full_data['total_sum_of_invoices_payout'],
                'title' => __('Total Payouts '),
                'icon' => 'file-invoice-dollar',
                'sub_msg' => 'Sum of the payouts ',
            ]);

            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'white',
                'size' => 2,
                'count' => $invoices_full_data['total_sum_of_invoices_fees'],
                'title' => __('Total Invoices Fees '),
                'icon' => 'file-invoice-dollar',
                'sub_msg' => 'Total of fees till now  ',
            ]);

            echo $this->element('Utils.backend/widgets/count_block',[
                'color' => 'dark',
                'size' => 2,
                'count' => $invoices_full_data['total_sum_of_invoices_cod'],
                'title' => __('Total of COD '),
                'icon' => 'file-invoice-dollar',
                'sub_msg' => 'Total sum of the CODS till now  ',
            ]);
//
//            echo $this->element('Utils.backend/widgets/count_block',[
//                'color' => 'light',
//                'size' => 2,
//                'count' => $invoices_full_data['total_active_orders_fees'],
//                'title' => __('Total UpComing Fees '),
//                'icon' => 'file-invoice-dollar',
//                'sub_msg' => 'Fees count for active ',
//            ]);
//
//            echo $this->element('Utils.backend/widgets/count_block',[
//                'color' => 'gray',
//                'size' => 2,
//                'count' => $invoices_full_data['total_due_orders_count'],
//                'title' => __('Due Orders '),
//                'icon' => 'file-invoice-dollar',
//                'sub_msg' => 'On hold orders 3 days ago ',
//            ]);
//
//            echo $this->element('Utils.backend/widgets/count_block',[
//                'color' => 'blue',
//                'size' => 2,
//                'count' => $invoices_full_data['total_due_orders_cods'],
//                'title' => __('Due Cods '),
//                'icon' => 'file-invoice-dollar',
//                'sub_msg' => 'COD for due orders 3 days ago  ',
//            ]);
//
//            echo $this->element('Utils.backend/widgets/count_block',[
//                'color' => 'red',
//                'size' => 2,
//                'count' => $invoices_full_data['total_due_orders_fees'],
//                'title' => __('Due Fees  '),
//                'icon' => 'file-invoice-dollar',
//                'sub_msg' => 'Fees for on hold orders 3 days ago  ',
//            ]);

            //
            ?>
        </div>

<div class="row">

<section class="col-lg-6 connectedSortable">
    <?php
    echo $this->element('Utils.backend/widgets/chart_dimminsions',[
        'canv_id' => 'chartscanv',
        'height' => '600',
        'title' => 'Invoices Since account creation  ',
        'icon' => 'file-invoice-dollar',
        'chart_type' => 'bar',
        'cahrtData' => $invoices_full_data['invoices_timeline'],
        'scale' =>'EGP'
    ]);

    ?>
</section>


<section class="col-lg-6 connectedSortable">
    <?php
    echo $this->element('Utils.backend/widgets/chart_dimminsions',[
        'canv_id' => 'chartscanv2',
        'height' => '600',
        'title' => 'Invoices Since account creation  ',
        'icon' => 'file-invoice-dollar',
        'chart_type' => 'pie',
        'cahrtData' => $invoices_full_data['invoices_timeline'],
        'scale' =>'EGP'
    ]);

    ?>
</section>

</div>


<?php
//echo $this->element('Utils.backend/widgets/data_table',[
//    'color' => 'green',
//    'size' => '12',
//    'table_data' => $invoices_full_data['total_orders_statues_table'],
//    'title' => __('Order Statues '),
//    'icon' => 'file-invoice-dollar',
//    'sub_msg' => 'Order Status List ' ,
//
//]);
?>
