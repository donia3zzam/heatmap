<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Location $location
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('Edit Location'), ['action' => 'edit', $location->id], ['class' => 'nav-link']) ?></li>
<li><?= $this->Form->postLink(__('Delete Location'), ['action' => 'delete', $location->id], ['confirm' => __('Are you sure you want to delete # {0}?', $location->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Locations'), ['action' => 'index'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('New Location'), ['action' => 'add'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Pickups'), ['controller' => 'Pickups', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Pickup'), ['controller' => 'Pickups', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="locations view large-9 medium-8 columns content">
    <h3><?= h($location->name) ?></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th scope="row"><?= __('User') ?></th>
                <td><?= $location->has('user') ? $this->Html->link($location->user->id, ['controller' => 'Users', 'action' => 'view', $location->user->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($location->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('City') ?></th>
                <td><?= h($location->city) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Building') ?></th>
                <td><?= h($location->building) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Floor') ?></th>
                <td><?= h($location->floor) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Apt') ?></th>
                <td><?= h($location->apt) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Contact Name') ?></th>
                <td><?= h($location->contact_name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Contact Phone') ?></th>
                <td><?= h($location->contact_phone) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Contact Email') ?></th>
                <td><?= h($location->contact_email) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($location->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($location->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($location->modified) ?></td>
            </tr>
        </table>
    </div>
    <div class="row">
        <h4><?= __('Address Line') ?></h4>
        <?= $this->Text->autoParagraph(h($location->address_line)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Pickups') ?></h4>
        <?php if (!empty($location->pickups)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('User Id') ?></th>
                    <th scope="col"><?= __('Location Id') ?></th>
                    <th scope="col"><?= __('Pickup Date') ?></th>
                    <th scope="col"><?= __('Pickup Time') ?></th>
                    <th scope="col"><?= __('Contact Name') ?></th>
                    <th scope="col"><?= __('Contact Phone') ?></th>
                    <th scope="col"><?= __('Contact Email') ?></th>
                    <th scope="col"><?= __('Description') ?></th>
                    <th scope="col"><?= __('Statues') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($location->pickups as $pickups): ?>
                <tr>
                    <td><?= h($pickups->id) ?></td>
                    <td><?= h($pickups->user_id) ?></td>
                    <td><?= h($pickups->location_id) ?></td>
                    <td><?= h($pickups->pickup_date) ?></td>
                    <td><?= h($pickups->pickup_time) ?></td>
                    <td><?= h($pickups->contact_name) ?></td>
                    <td><?= h($pickups->contact_phone) ?></td>
                    <td><?= h($pickups->contact_email) ?></td>
                    <td><?= h($pickups->description) ?></td>
                    <td><?= h($pickups->statues) ?></td>
                    <td><?= h($pickups->created) ?></td>
                    <td><?= h($pickups->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'Pickups', 'action' => 'view', $pickups->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'Pickups', 'action' => 'edit', $pickups->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Form->postLink( __('Delete'), ['controller' => 'Pickups', 'action' => 'delete', $pickups->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pickups->id), 'class' => 'btn btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <?php endif; ?>
    </div>
</div>
