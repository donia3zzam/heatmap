<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Point $point
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('Edit Point'), ['action' => 'edit', $point->id], ['class' => 'nav-link']) ?></li>
<li><?= $this->Form->postLink(__('Delete Point'), ['action' => 'delete', $point->id], ['confirm' => __('Are you sure you want to delete # {0}?', $point->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Points'), ['action' => 'index'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('New Point'), ['action' => 'add'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('List Events'), ['controller' => 'Events', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Event'), ['controller' => 'Events', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Sensors'), ['controller' => 'Sensors', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Sensor'), ['controller' => 'Sensors', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Halls'), ['controller' => 'Halls', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Hall'), ['controller' => 'Halls', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="points view large-9 medium-8 columns content">
    <h3><?= h($point->id) ?></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th scope="row"><?= __('Mac') ?></th>
                <td><?= h($point->mac) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Event') ?></th>
                <td><?= $point->has('event') ? $this->Html->link($point->event->name, ['controller' => 'Events', 'action' => 'view', $point->event->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Sensor') ?></th>
                <td><?= $point->has('sensor') ? $this->Html->link($point->sensor->name, ['controller' => 'Sensors', 'action' => 'view', $point->sensor->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Hall') ?></th>
                <td><?= $point->has('hall') ? $this->Html->link($point->hall->name, ['controller' => 'Halls', 'action' => 'view', $point->hall->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($point->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('At Time') ?></th>
                <td><?= $this->Number->format($point->at_time) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Distance') ?></th>
                <td><?= $this->Number->format($point->distance) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($point->created) ?></td>
            </tr>
        </table>
    </div>
</div>
