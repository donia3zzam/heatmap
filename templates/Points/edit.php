<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Point $point
 * @var \App\Model\Entity\Event[]|\Cake\Collection\CollectionInterface $events
 * @var \App\Model\Entity\Sensor[]|\Cake\Collection\CollectionInterface $sensors
 * @var \App\Model\Entity\Hall[]|\Cake\Collection\CollectionInterface $halls
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $point->id], ['confirm' => __('Are you sure you want to delete # {0}?', $point->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Points'), ['action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Events'), ['controller' => 'Events', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Event'), ['controller' => 'Events', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Sensors'), ['controller' => 'Sensors', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Sensor'), ['controller' => 'Sensors', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Halls'), ['controller' => 'Halls', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Hall'), ['controller' => 'Halls', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="points form content">
    <?= $this->Form->create($point) ?>
    <fieldset>
        <legend><?= __('Edit Point') ?></legend>
        <?php
            echo $this->Form->control('mac');
            echo $this->Form->control('at_time');
            echo $this->Form->control('distance');
            echo $this->Form->control('event_id', ['options' => $events]);
            echo $this->Form->control('sensor_id', ['options' => $sensors]);
            echo $this->Form->control('hall_id', ['options' => $halls]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
