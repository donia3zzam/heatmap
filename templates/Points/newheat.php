<script src="https://cdnjs.cloudflare.com/ajax/libs/heatmap.js/2.0.0/heatmap.min.js" integrity="sha512-FpvmtV53P/z7yzv1TAIVH7PNz94EKXs5aV6ts/Zi+B/VeGU5Xwo6KIbwpTgKc0d4urD/BtkK50IC9785y68/AA==" crossorigin="anonymous"></script>
<?php
$imageH = $hallData->height * 10;
$imageW = $hallData->width * 10;
?>
<div class="overlay"></div>
       
      <p>
  <a class="btn btn-primary collapsed m-5 " data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">search
  <i class="fas fa-search"></i>
  </a>
 
</p>
<div class="collapse" id="collapseExample" style="">
  <div class="card card-body">
  <form>
    <div class="row">       
        <div class="col">
            <?php echo $this->Form->control('datetime',[ 'label' => 'Start Day' ,'type'=>'datetime','id'=>'start_datetime', 'value'=>$start_datetime, 'min'=>$start_datetime, 'max'=>$end_datetime]); ?>
        </div>
        <div class="col">
            <?php echo $this->Form->control('datetime',[ 'label' => 'End Day' ,'type'=>'datetime','id'=>'datetime_end', 'value'=>$start_datetime, 'min'=>$start_datetime, 'max'=>$end_datetime]); ?>
        </div>

        <div class="col">
            <?php echo $this->Form->control('mac_address',[ 'label' => 'Search For Mac','id'=>'mac-address' ]); ?>
        </div>

        <div class="col">
            <?php echo $this->Form->control('time_slip',[ 'label' => 'time lapse in min ','id'=>'time_lapse','value'=>1,'type'=>'number' ]); ?>
        </div>

        <div class="col">
            <?php echo $this->Form->control('density',[ 'label' => 'density','id'=>'map_density','value'=>1,'type'=>'number' ]); ?>
        </div>
        <div class="col">
            <br/>
            <button type="button" id="search_button" class="btn btn-secondary">Search</button>
        </div>
    </div>
</form>
  </div>
</div>

  
<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sensor[]|\Cake\Collection\CollectionInterface $sensors
 */

?>
<!-- Ashraaaaaaaaaaaaaaaf CODE -->
<div class="row">
    
    <div class="col-lg-4">
        <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-0">total unique visitors </h5>
                        <span class="h2 font-weight-bold mb-0"><span id="unique-counter">0</span> </span>
                    </div>
                    <div class="col-auto">
                        <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                            <i class="ni ni-active-40"></i>
                        </div>
                    </div>
                </div>
                <p class="mt-3 mb-0 text-sm">

                </p>
            </div>
        </div>
        </div>
            <div class="col-lg-4">
        <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-0">Time average </h5>
                        <span class="h2 font-weight-bold mb-0"><span id="min-count">0</span> Mins</span>
                    </div>
                    <div class="col-auto">
                        <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                            <i class="ni ni-active-40"></i>
                        </div>
                    </div>
                </div>
                <p class="mt-3 mb-0 text-sm">

                </p>    
            </div>
        </div>
        </div>
        <div class="col-xl-9">
        <div class="demo-wrapper ">

            <div id='heatMap'></div>
            <div class="tooltipxxxx"></div>
            <div class="legend-area">
                <button class="text-center btn btn-danger button-count">Visitors Count</button>
                <span id="min"></span>
                <span id="max"></span>
                <img id="gradient" src="" style="width:50%" />
            </div>
        </div>
    </div>
    </div>
</div>
<div class="d-flex justify-content-center mb-4 ">

  
  <div>
    <a href="#">
    <i class="fas fa-angle-double-left"></i>
    </a>
  </div>
  <div>
    <a href="#">
    <i class="fas fa-play"></i>
    </a>
  </div>
  
  <div>
    <a href="#">
    <i class="fas fa-angle-double-right"></i>
    </a>
  </div>
  
 
</div>

<div class="progress d-flex justify-content-center mb-4 ">
  <div class="progress-bar" role="progressbar" style="width: 41%" aria-valuenow="41" aria-valuemin="0" aria-valuemax="100"></div>
</div>
<!-- END Ashraaaaaaaf CODE -->
  

<style>
    #heatMap {
        background-image: url(<?=$hallData->floor_image?>);
        height: <?=$imageH?>px;
        width: <?=$imageW?>px;
        background-size:contain ;
        
    }
    .tooltipxxxx { position:absolute; left:0; top:0; background:rgba(0,0,0,.8); color:white; font-size:14px; padding:5px; line-height:18px; display:none;}
    .demo-wrapper { position:relative; }
    .legend-area {  padding:10px; margin-bottom:20px; background:white;  line-height:1em; }
    .button-count {  margin-bottom:70px; position:absolute; bottom:-8%; right:21%; }
    #min { float:left; }
    #max { float:right; }
    span { font-size:14px; margin:0; padding:0; }
    .tooltip { position:absolute; left:0; top:0; background:rgba(0,0,0,.8); color:white; font-size:14px; padding:5px; line-height:18px; display:none;}
    .demo-wrapper { position:relative; }

    /*****Ashraf Css */
    
.progress{
    width: 20%;
    position: absolute;
    left: 40%;
}
.fas{
    /* font-size:40px; */
    padding:0 10px;
}

a {
  display: inline-block;
  border-radius: 50%;
}

/* a:hover .left, a:hover .top, a:hover .bottom, a:hover .right{
  border: 0.5em solid #5E72E4;
}

a:hover .left:after, a:hover .top:after, a:hover .bottom:after, a:hover .right:after {
  border-top: 0.5em solid #5E72E4;
  border-right: 0.5em solid #5E72E4;
}

.left {
  display: inline-block;
  width: 4em;
  height: 4em;
  border: 0.5em solid #333;
  border-radius: 50%;
  margin-right: 1.5em;
}

.left:after {
  content: '';
	display: inline-block;
  margin-top: 1.09em;
  margin-left: 0.9em;
  width: 1.4em;
  height: 1.4em;
  border-top: 0.5em solid #333;
  border-right: 0.5em solid #333;
  -moz-transform: rotate(-135deg);
  -webkit-transform: rotate(-135deg);
  transform: rotate(-135deg);
}
.play {
  display: inline-block;
  width: 4em;
  height: 4em;
  border: 0.5em solid #333;
  border-radius: 50%;
  margin-right: 1.5em;
}

.play:after {
  content: '';
	display: inline-block;
  margin-top: 1.09em;
  margin-left: 0.9em;
  width: 1.4em;
  height: 1.4em;
  border-top: 0.5em solid #333;
  border-right: 0.5em solid #333;
  -moz-transform: rotate(-135deg);
  -webkit-transform: rotate(-135deg);
  transform: rotate(-135deg);
}





.right {
  display: inline-block;
  width: 4em;
  height: 4em;
  border: 0.5em solid #333;
  border-radius: 50%;
  margin-left: 1.5em;
}

.right:after {
  content: '';
	display: inline-block;
  margin-top: 1em;
  margin-left: .8em;
  width: 1.4em;
  height: 1.4em;
  border-top: 0.5em solid #333;
  border-right: 0.5em solid #333;
  -moz-transform: rotate(45deg);
  -webkit-transform: rotate(45deg);
  transform: rotate(45deg);
} */

</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>

<script>

    $(document).ready(function(){
        $('#search_button').click(function () {
//            console.log(delete heatmapInstance);
//            console.log(delete points);
//            console.log(delete data);
//
//            $time = new Date($('#start_datetime').val()).getTime() / 1000
//            $endTime = new Date($('#datetime_end').val()).getTime() / 1000
//            $searchedMac = $('#mac-address').val() == '' ? '' : '/'+$('#mac-address').val() +'/';
//            checkmac = $('#mac-address').val() == '' ? false : true;
//            $value_for_count = parseInt( $('#map_density').val() );
//            $time_lapse = $('#time_lapse').val();
//            $.ajax({
//                url: "<?//=ROOT_URL?>///points/heatmap_by_time/"+<?//=$hallData->id?>//+"/"+$time+"/"+$endTime+"/"+$time_lapse+"/"+$searchedMac,
//                context: document.body
//            }).done(function($jsonData) {
//                $('#unique-counter').text(JSON.parse($jsonData).uniquex);
//                $('#min-count').text(JSON.parse($jsonData).average);
//                $jsonData = JSON.parse($jsonData).data;
//
//                console.log($value_for_count);
//
//                var heatmapInstance = h337.create({
//                    // only container is required, the rest will be defaults
//                    container: document.querySelector('#heatMap')
//                });
//
//                /* tooltip code start */
//                var demoWrapper = document.querySelector('.demo-wrapper');
//                var tooltip = document.querySelector('.tooltipxxxx');
//                function updateTooltip(x, y, value) {
//                    // + 15 for distance to cursor
//                    var transl = 'translate(' + (x + 15) + 'px, ' + (y + 15) + 'px)';
//                    tooltip.style.webkitTransform = transl;
//                    tooltip.innerHTML = value;
//                };
//
//                demoWrapper.onmousemove = function(ev) {
//                    var x = ev.layerX;
//                    var y = ev.layerY;
//                    // getValueAt gives us the value for a point p(x/y)
//                    var value = heatmapInstance.getValueAt({
//                        x: x,
//                        y: y
//                    });
//                    tooltip.style.display = 'block';
//                    updateTooltip(x, y, value);
//                };
//// hide tooltip on mouseout
//                demoWrapper.onmouseout = function() {
//                    tooltip.style.display = 'none';
//                };
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//                    // now generate some random data
//                    var points = [];
//                    var max = 99;
//                    var width = <?//=$imageW?>//;
//                var height = <?//=$imageH?>//;
//                var len = 200;
//
//                $jsonData.forEach(function(xxx){
//                    var val = Math.floor(Math.random()*100);
//                    var point = {
//                        x: xxx.x,
//                        y: xxx.y,
//                        value: $value_for_count
//                    };
//                    points.push(point);
//                });
//                // heatmap data format
//                var data = {
//                    max: max,
//                    data: points
//                };
//                // if you have a set of datapoints always use setData instead of addData
//                // for data initialization
//                heatmapInstance.setData(data);
//                console.log(data);
//                setTimeout(function (){
//                    heatmapInstance.repaint();
//                },100);
//
//
//
//            });

            var legendCanvas = document.createElement('canvas');
            legendCanvas.width = 100;
            legendCanvas.height = 10;
            var min = document.querySelector('#min');
            var max = document.querySelector('#max');
            var gradientImg = document.querySelector('#gradient');
            var legendCtx = legendCanvas.getContext('2d');
            var gradientCfg = {};
            function updateLegend(data) {
                // the onExtremaChange callback gives us min, max, and the gradientConfig
                // so we can update the legend
                min.innerHTML = data.min;
                max.innerHTML = data.max;
                // regenerate gradient image
                if (data.gradient != gradientCfg) {
                    gradientCfg = data.gradient;
                    var gradient = legendCtx.createLinearGradient(0, 0, 100, 1);
                    for (var key in gradientCfg) {
                        gradient.addColorStop(key, gradientCfg[key]);
                    }
                    legendCtx.fillStyle = gradient;
                    legendCtx.fillRect(0, 0, 100, 10);
                    gradientImg.src = legendCanvas.toDataURL();
                }
            };
            /* legend code end */
            var heatmapInstance = h337.create({
                container: document.querySelector('#heatMap'),
                // onExtremaChange will be called whenever there's a new maximum or minimum
                onExtremaChange: function(data) {
                    updateLegend(data);
                }
            });
            /* tooltip code start */
            var demoWrapper = document.querySelector('.demo-wrapper');
            var tooltip = document.querySelector('.tooltipxxxx');
            function updateTooltip(x, y, value) {
                // + 15 for distance to cursor
                var transl = 'translate(' + (x + 15) + 'px, ' + (y + 15) + 'px)';
                tooltip.style.webkitTransform = transl;
                tooltip.innerHTML = value;
            };
            demoWrapper.onmousemove = function(ev) {
                var x = ev.layerX;
                var y = ev.layerY;
                // getValueAt gives us the value for a point p(x/y)
                var value = heatmapInstance.getValueAt({
                    x: x,
                    y: y
                });
                tooltip.style.display = 'block';
                updateTooltip(x, y, value);
            };
// hide tooltip on mouseout
            demoWrapper.onmouseout = function() {
                tooltip.style.display = 'none';
            };
            /* tooltip code end */

// generate some random data
            var points = [];
            var max = 0;
            var min = 1234;
            var width = <?=$imageW?>;
            var height = <?=$imageH?>;
            var len = 200;

            while (len--) {
                var val = Math.floor(Math.random()*1234);
                max = Math.max(max, val);
                min = Math.min(min, val);
                var point = {
                    x: Math.floor(Math.random()*width),
                    y: Math.floor(Math.random()*height),
                    value: val
                };
                points.push(point);
            }
            var data = { max: max, min:min, data: points };
            heatmapInstance.setData(data);
        })
        $(document).on({
            ajaxStart: function(){
                $("body").addClass("loading");
                $('.overlay').show();
            },
            ajaxStop: function(){
                $("body").removeClass("loading");
                $('.overlay').hide();

            }
        });
    })




</script>


<style>
    .overlay{
        display: none;
        position: fixed;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        z-index: 999;
        background: rgba(255,255,255,0.8) url("<?=ROOT_URL?>/img/45.gif") center no-repeat;
    }
    /* Turn off scrollbar when body element has the loading class */
    body.loading{
        overflow: hidden;
    }
    /* Make spinner image visible when body element has the loading class */
    body.loading .overlay{
        display: block;
    }
</style>
