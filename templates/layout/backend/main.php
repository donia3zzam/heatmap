<?=$this->element('backend/header')?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    <?=$this->element('backend/navbar')?>

    <?=$this->element('backend/left_navbar')?>


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark"><?=__($this->getRequest()->getAttribute('params')['controller'] )?></h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <div class="breadcrumb float-sm-right">
                            <div class="btn-group-fab" role="group" aria-label="FAB Menu">
                                <div>
                                    <button type="button" class="btn btn-main btn-primary has-tooltip" data-placement="left" title="Menu"> <i class="fa fa-plus"></i> </button>
                                    <a href="<?=ROOT_URL?>orders/add" class="btn btn-sub btn-info has-tooltip" data-placement="left" title="Fullscreen"> Shipment <i class="fas fa-shipping-fast"></i> </a>
                                    <a href="<?=ROOT_URL?>pickups/add" class="btn btn-sub btn-danger has-tooltip" data-placement="left" title="Save"> Pickups <i class="fas fa-dolly-flatbed"></i> </a>
                                    <a href="<?=ROOT_URL?>orders/upload" class="btn btn-sub btn-warning has-tooltip" data-placement="left" title="Download"> Upload <i class="fa fa-upload"></i> </a>
                                </div>
                            </div>

                        </div>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <h2 class="FalshMsg"><?= $this->Flash->render() ?></h2>

                <?= $this->fetch('content'); ?>

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <?=$this->element('backend/footer')?>
