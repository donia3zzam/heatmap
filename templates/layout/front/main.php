<?=$this->element('frontend/header')?>
<?=$this->element('frontend/top_nav')?>

<?php
if (!isset($hide_search)){
    echo $this->element('frontend/search');
}?>

<?php

if (!isset($hide_cats)){
    echo $this->element('frontend/categories_grid');
}
?>


<!-- Start Featured Section -->
    <?= $this->Flash->render() ?>
    <?= $this->fetch('content') ?>
<!-- END SECTION Featured-->
<?=$this->element('frontend/footer')?>
