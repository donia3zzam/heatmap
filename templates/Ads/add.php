<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Ad $ad
 * @var \App\Model\Entity\Category[]|\Cake\Collection\CollectionInterface $categories
 * @var \App\Model\Entity\Location[]|\Cake\Collection\CollectionInterface $locations
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 * @var \App\Model\Entity\Favorite[]|\Cake\Collection\CollectionInterface $favorites
 * @var \App\Model\Entity\Field[]|\Cake\Collection\CollectionInterface $fields
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('List Ads'), ['action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Locations'), ['controller' => 'Locations', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Location'), ['controller' => 'Locations', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Favorites'), ['controller' => 'Favorites', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Favorite'), ['controller' => 'Favorites', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Fields'), ['controller' => 'Fields', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Field'), ['controller' => 'Fields', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<?php if(isset($_GET['cat_id'])){ ?>
<div class="ads form content">
    <?= $this->Form->create($ad) ?>
    <fieldset>
        <legend><?= __('Add Ad') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('slug');
            echo $this->Form->control('description');
            echo $this->Media->multi('image_folder');
//            echo $this->Form->control('image_folder');
            echo $this->Form->control('category_id', ['options' => $subCategory]);
            echo $this->Form->control('location_id', ['options' => $locations]);
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('active');
            echo $this->Form->control('statues');
            echo $this->Form->control('type');
            echo $this->Form->control('active_fo_days');
            echo $this->Form->control('featured');
            echo $this->Form->control('views');
            echo $this->Form->control('rate');
            $this->CustomFields->FormBuilderForCustomField($_GET['cat_id']);

        //            echo $this->Form->control('fields._ids', ['options' => $fields]);
                ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>

    <?php } else {
    echo "<h2> Select Main Category </h2>";
        foreach ($categories as $id => $cat){
            echo $this->Html->link($cat,ROOT_URL."ads/add?cat_id=".$id,['class' => 'btn btn-primary m-2']);
        }
    } ?>
</div>


