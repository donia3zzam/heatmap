<link rel="stylesheet" href="<?=ROOT_URL?>frontend/css/form.css">

<section id="register"  class="p-5 ">
    <div class="container">
        <div class="card  p-5 w-50">
            <h4 class="card-title text-center">Register</h4>
            <form>
                <div class="input-group mt-3">
                    <input type="text" class="form-control bg-light" placeholder="User Name">
                    <div class="input-group-append" >
                          <span class="input-group-text">
                            <i class="fas fa-user " style="color: 315d78;"></i>
                          </span>
                    </div>
                </div>
                <div class="input-group mt-4">
                    <input type="text" class="form-control bg-light" placeholder="User Email">
                    <div class="input-group-append" >
                          <span class="input-group-text">
                          <i class="fas fa-envelope " style="color: 315d78;"></i>
                        </span>
                    </div>
                </div>
                <div class="input-group mt-4">
                    <input type="password" class="form-control bg-light" placeholder="Password">
                    <div class="input-group-append" >
                          <span class="input-group-text">
                            <i class="fas fa-lock " style="color: 315d78;"></i>
                        </span>
                    </div>
                </div>
                <div class="input-group mt-4">
                    <input type="password" class="form-control bg-light" placeholder="Confirm Password">
                    <div class="input-group-append" >
                          <span class="input-group-text">
                            <i class="fas fa-lock " style="color: 315d78;"></i>
                        </span>
                    </div>
                </div>
            </form>
            <button type="button" class="btn btn-danger hvr-pulse-grow btn-lg btn-block mt-4">Submit</button>
        </div>
    </div>
</section>
