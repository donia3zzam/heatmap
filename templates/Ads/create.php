<section id="add-post" class="mt-5 p-5 ">
    <div class="container ">
        <div class="row">
            <h3 class="head text-secondary">Place an Ad <i class="fas fa-ad"></i></h3>
        </div>
        <hr>

        <?= $this->Form->create($ad,[
            'align' => [
                'sm' => [
                    'left' => 6,
                    'middle' => 6,
                    'right' => 12
                ],
                'md' => [
                    'left' => 4,
                    'middle' => 4,
                    'right' => 4
                ]
                ]
        ]) ?>
        <fieldset>
            <legend><?= __('Add Ad') ?></legend>
            <?php
            echo $this->Form->control('name');
            echo $this->Form->control('slug');
            echo $this->Form->control('description');
            echo $this->Media->multi('image_folder');
            //            echo $this->Form->control('image_folder');
            echo $this->Form->control('category_id', ['options' => $subCategory]);
            echo $this->Form->control('location_id', ['options' => $locations]);
            echo $this->Form->control('user_id', [ 'type'=>'hidden' ,'value' => 1]);

            $this->CustomFields->FormBuilderForCustomField($_GET['cat_id']);

            //            echo $this->Form->control('fields._ids', ['options' => $fields]);
            ?>
        </fieldset>
        <?= $this->Form->button(__('Submit')) ?>
        <?= $this->Form->end() ?>


<!--        <div class="container ">-->
<!--            <form>-->
<!--                <div class="form-group row ml-5">-->
<!--                    <label for="inputEmail3" class="col-sm-2 col-form-label">Title</label>-->
<!--                    <div class="col-sm-10">-->
<!--                        <input type="email" class="form-control w-50" id="inputEmail3">-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="form-group row ml-5">-->
<!--                    <label for="inputEmail3" class="col-sm-2 col-form-label ">Main category</label>-->
<!--                    <div class="col-sm-10">-->
<!--                        <select class="custom-select w-50">-->
<!--                            <option selected>Open this select menu</option>-->
<!--                            <option value="1">One</option>-->
<!--                            <option value="2">Two</option>-->
<!--                        </select>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </form>-->
<!--            <hr>-->
<!--            <form>-->
<!--                <div class="form-group row ml-5">-->
<!--                    <label for="inputEmail3" class="col-sm-2 col-form-label">Description</label>-->
<!--                    <div class="col-sm-10 ">-->
<!--                        <textarea class="form-control w-50" id="exampleFormControlTextarea1" rows="4"></textarea>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="form-group row ml-5">-->
<!--                    <label for="inputEmail3" class="col-sm-2 col-form-label">Chosse image</label>-->
<!--                    <div class="col-sm-10 ">-->
<!--                        <div class="custom-file">-->
<!--                            <input type="file" class="custom-file-input" id="customFile">-->
<!--                            <label class="custom-file-label w-50" for="customFile"></label>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </form>-->
<!--            <hr>-->
<!--            <form>-->
<!--                <div class="form-group row ml-5">-->
<!--                    <label for="inputEmail3" class="col-sm-2 col-form-label">Ad Location</label>-->
<!--                    <div class="col-sm-10 ">-->
<!--                        <input type="email" class="form-control w-50" id="inputEmail3">-->
<!--                    </div>-->
<!--                </div>-->
<!--            </form>-->
<!--            <hr>-->
<!--            <form>-->
<!--                <div class="form-group row ml-5">-->
<!--                    <label for="inputEmail3" class="col-sm-2 col-form-label">Name</label>-->
<!--                    <div class="col-sm-10 ">-->
<!--                        <input type="email" class="form-control w-50" id="inputEmail3">-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="form-group row ml-5">-->
<!--                    <label for="inputEmail3" class="col-sm-2 col-form-label">Mobile phone number</label>-->
<!--                    <div class="col-sm-10 ">-->
<!--                        <input type="email" class="form-control w-50" id="inputEmail3">-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="form-group row ml-5">-->
<!--                    <label for="inputEmail3" class="col-sm-2 col-form-label">Email address</label>-->
<!--                    <div class="col-sm-10 ">-->
<!--                        <input type="email" class="form-control w-50" id="inputEmail3">-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="form-group row ml-5">-->
<!--                    <label for="inputEmail3" class="col-sm-2 col-form-label">Method of communication</label>-->
<!--                    <div class="col-sm-10 ">-->
<!--                        <div class="form-check form-check-inline">-->
<!--                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">-->
<!--                            <label class="form-check-label" for="inlineRadio1">Both</label>-->
<!--                        </div>-->
<!--                        <div class="form-check form-check-inline">-->
<!--                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">-->
<!--                            <label class="form-check-label" for="inlineRadio2">Phone</label>-->
<!--                        </div>-->
<!--                        <div class="form-check form-check-inline">-->
<!--                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3">-->
<!--                            <label class="form-check-label" for="inlineRadio3">Chat</label>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </form>-->
<!--            <hr>-->
<!--            <a href="myaccount.html"> <button type="button" class="btn   btn-sm ml-5 hvr-pulse-grow">Submit</button> </a>-->
<!--        </div>-->
    </div>
</section>
