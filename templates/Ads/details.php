<!-- Start Product -->
<section class="product">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 ">
                <div id="slider" class="mt-5">
                    <h4 class="card-title mt-4 "><?=$ad->name?></h4>
                    <div class="meta d-flex flex-row ">
                        <div class="p-2"><i class="fas fa-map-marker-alt"></i> <span><?=$ad->location['name']?></span></div>
                        <div class="p-2"><i class="fas fa-clock"></i> <span><?=$ad->created?></span></div>
                        <div class="p-2"><i class="fas fa-eye"></i> <span> Views <?=$ad->views?></span></div>
                    </div>

                    <div id="carousel-Controls" class="carousel slide mt-4 mb-4" data-ride="carousel">
                        <div class="carousel-inner">
                            <?php
                                foreach ($ad->images_list as $key => $image){
                                    ?>
                                    <div class="carousel-item <?= $key ==0?"active":""?>">
                                        <img class="d-block w-100" src="<?=$image?>" alt="slide <?=$key?>">
                                    </div>
                            <?php
                                }
                            ?>


                        </div>
                        <a href="" ><i class="fas fa-expand"></i></a>
                        <a class="carousel-control-prev" href="#carousel-Controls" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel-Controls" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
<!--                        <ol class="carousel-indicators list-inline">-->
<!--                            <li class="list-inline-item active">-->
<!--                                <a id="carousel-selector-0" class="selected" data-slide-to="0" data-target="#carousel-Controls">-->
<!--                                    <img src="--><?//=ROOT_URL?><!--/frontend/image/8-5.jpg" class="img-fluid">-->
<!--                                </a>-->
<!--                            </li>-->
<!--                            <li class="list-inline-item">-->
<!--                                <a id="carousel-selector-1" data-slide-to="1" data-target="#carousel-Controls">-->
<!--                                    <img src="--><?//=ROOT_URL?><!--/frontend/image/9-7.jpg" class="img-fluid">-->
<!--                                </a>-->
<!--                            </li>-->
<!--                            <li class="list-inline-item">-->
<!--                                <a id="carousel-selector-2" data-slide-to="2" data-target="#carousel-Controls">-->
<!--                                    <img src="--><?//=ROOT_URL?><!--/frontend/image/7-7.jpg" class="img-fluid">-->
<!--                                </a>-->
<!--                            </li>-->
<!--                            <li class="list-inline-item">-->
<!--                                <a id="carousel-selector-2" data-slide-to="3" data-target="#carousel-Controls">-->
<!--                                    <img src="--><?//=ROOT_URL?><!--/frontend/image/6-7.jpg" class="img-fluid">-->
<!--                                </a>-->
<!--                            </li>-->
<!--                            <li class="list-inline-item">-->
<!--                                <a id="carousel-selector-2" data-slide-to="4" data-target="#carousel-Controls">-->
<!--                                    <img src="--><?//=ROOT_URL?><!--/frontend/image/2-9-oc7e7iga33lgrwtvn63rs09eitv0xf2n8vz74cbpmo.jpg" class="img-fluid">-->
<!--                                </a>-->
<!--                            </li>-->
<!--                        </ol>-->
                    </div>

                    <div class="row p-3 mt-5 bg-white">
                        <div class="col-lg-6">
                            <div class="property">
                                <span class="name">Bathrooms :</span>
                                <span class="value float-right">2</span>
                            </div>
                            <hr>
                            <div class="property">
                                <span class="name ">Garage :</span>
                                <span class="value float-right">2</span>
                            </div>
                            <hr>
                            <div class="property">
                                <span class="name ">Garage Size :</span>
                                <span class="value float-right">200 SqFt</span>
                            </div>
                            <hr>
                            <div class="property">
                                <span class="name ">Year Built :</span>
                                <span class="value float-right">2016-01-09</span>
                            </div>
                            <hr>
                        </div>
                        <div class="col-lg-6">
                            <div class="property">
                                <span class="name ">Property Type :</span>
                                <span class="value float-right">Single Family</span>
                            </div>
                            <hr>
                            <div class="property">
                                <span class="name ">Property Size :</span>
                                <span class="value float-right">1200 Sq Ft</span>
                            </div>
                            <hr>
                            <div class="property">
                                <span class="name ">Bedrooms :</span>
                                <span class="value float-right">2</span>
                            </div>
                            <hr>
                            <div class="property">
                                <span class="name ">Seller Type :</span>
                                <span class="value float-right">Type:Personal</span>
                            </div>
                            <hr>
                        </div>
                    </div>
                </div>
                <!--property-features-->
                <div id="property-features">
                    <!--                    <div class="container">-->
                    <!--                        <div class="row ">-->
                    <!--                            <div class="col-lg-8 p-2  bg-white">-->
                    <h6 class="ml-3">Property Features</h6>
                    <div class="row features mt-4">
                        <div class="col-lg-4">
                            <ul>
                                <li>
                                    <i class="far fa-check-circle"></i>
                                    <span>Air</span>
                                </li>
                                <li>
                                    <i class="far fa-check-circle"></i>
                                    <span>Air</span>
                                </li>
                                <li>
                                    <i class="far fa-check-circle"></i>
                                    <span>Air</span>
                                </li>
                                <li>
                                    <i class="far fa-check-circle"></i>
                                    <span>Air</span>
                                </li>
                                <li>
                                    <i class="far fa-check-circle"></i>
                                    <span>Air</span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-4">
                            <ul>
                                <li>
                                    <i class="far fa-check-circle"></i>
                                    <span>Air</span>
                                </li>
                                <li>
                                    <i class="far fa-check-circle"></i>
                                    <span>Air</span>
                                </li>
                                <li>
                                    <i class="far fa-check-circle"></i>
                                    <span>Air</span>
                                </li>
                                <li>
                                    <i class="far fa-check-circle"></i>
                                    <span>Air</span>
                                </li>
                                <li>
                                    <i class="far fa-check-circle"></i>
                                    <span>Air</span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-4">
                            <ul>
                                <li>
                                    <i class="far fa-check-circle"></i>
                                    <span>Air</span>
                                </li>
                                <li>
                                    <i class="far fa-check-circle"></i>
                                    <span>Air</span>
                                </li>
                                <li>
                                    <i class="far fa-check-circle"></i>
                                    <span>Air</span>
                                </li>
                                <li>
                                    <i class="far fa-check-circle"></i>
                                    <span>Air</span>
                                </li>
                                <li>
                                    <i class="far fa-check-circle"></i>
                                    <span>Air</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col  bg-white">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 p-3">
                                <div class="property-type">
                                    <span class="name ml-">Property Type :</span>
                                    <span class="value float-right">House</span>
                                </div>
                                <hr>
                            </div>
                        </div>
                    </div>
                    <h5 class="p-3">Description</h5>
                    <p class="description p-3 ">
                        <?=$ad->description?>
                    </p>
                </div>
                <!--CONTACT-->
                <div id="contact" class="mt-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 bg-white">
                                <div class="contact-head p-3 mt-2">
                                    <h5>Contact Information</h5>
                                </div>
                                <div class="ml-3">
                                    <div class="contact-info">
                                        <span class="name">Categories :</span>
                                        <span class="value ml-4"><a href="">Estate</a> <a href="">For rent</a> </span>
                                    </div>
                                    <hr>
                                    <div class="contact-info">
                                        <span class="name">Phone :</span>
                                        <span class="value ml-4"><a href="">999 888 777</a></span>
                                    </div>
                                    <hr>
                                    <div class="contact-info">
                                        <span class="name">Address :</span>
                                        <span class="value ml-4">Canada , canada</span>
                                    </div>
                                    <hr>
                                    <div class="contact-info">
                                        <span class="name">Listing Tags :</span>
                                        <span class="value ml-4"><a href="">Rent</a> <a href="">Office</a> </span>
                                    </div>
                                    <hr>
                                    <div class="contact-info">
                                        <span class="name">Website :</span>
                                        <span class="value ml-4"> <a href="">View our site</a></span>
                                    </div>
                                    <hr>
                                    <div class="contact-info">
                                        <span class="name">Email :</span>
                                        <span class="value ml-4"> <a href="">example@gmail.com</a></span>
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--VIDEOS-->
                <div id="video" class="mt-5">
                    <div class="container">
                        <div class="row">
<!--                            <div class="col-lg-12 bg-white">-->
<!--                                <h5 class="title p-3 mt-2">Videos</h5>-->
<!--                                <div class="embed-responsive embed-responsive-4by3">-->
<!--                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="review p-4">-->
<!--                                <h5 class="title mt-5">Post New Review</h5>-->
<!--                                <div class="col form-group mt-3">-->
<!--                                    <textarea class="form-control" placeholder="Review" rows="4"></textarea>-->
<!--                                </div>-->
<!--                                <div class="row text-center">-->
<!--                                    <div class="col-lg-4">-->
<!--                                        <div class="input-group mt-3">-->
<!--                                            <div class="input-group-append" >-->
<!--                                      <span class="input-group-text bg-white">-->
<!--                                        <i class="fas fa-user"></i>-->
<!--                                      </span>-->
<!--                                            </div>-->
<!--                                            <input type="text" class="form-control bg-white" placeholder="FULL NAME">-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <div class="col-lg-4">-->
<!--                                        <div class="input-group mt-3">-->
<!--                                            <div class="input-group-append" >-->
<!--                                        <span class="input-group-text bg-white">-->
<!--                                          <i class="fas fa-envelope"></i>-->
<!--                                        </span>-->
<!--                                            </div>-->
<!--                                            <input type="text" class="form-control bg-white" placeholder="EMAIL ADDRESS">-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <div class="col-lg-4">-->
<!--                                        <div class="input-group mt-3">-->
<!--                                            <div class="input-group-append" >-->
<!--                                        <span class="input-group-text bg-white">-->
<!--                                            <i class="fas fa-globe-europe"></i>-->
<!--                                        </span>-->
<!--                                            </div>-->
<!--                                            <input type="text" class="form-control bg-white" placeholder="WEBSITE">-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <input class="form-control review-title mt-5" type="text" placeholder="Review Title">-->
<!--                                </div>-->
<!--                                <button type="button" class="btn btn-success btn-sm mt-5">Submit Review</button>-->
<!--                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <aside id="side">
                    <div class="sidebar">
                        <section class="user-profile">
                            <div class="card text-center">

                                <img class="card-img-top" src="<?=ROOT_URL?>/frontend/image/user.jpg" alt="Card image cap">
                                <div class="card-body">
                                    <p class="card-title"><?=$ad['user']['username']?></p>
                                    <p class="card-text">
                                        Member <?=$ad['user']['created']?>
                                    </p>
                                    <a href="<?=ROOT_URL?>/ads/foruser/<?=$ad['user']['id']?>" class="btn btn-link text-white hvr-pulse-grow ">view all ads</a>
                                    <ul class="list-inline mt-2">
                                        <li class="list-inline-item">  <a href="#" class="btn btn-link btn-outline-secondary hvr-pulse-grow text-white">SEND MESSAGE</a></li>
                                        <li class="list-inline-item">  <a href="#" class="btn btn-link btn-outline-secondary hvr-pulse-grow text-white">SEND OFFER</a></li>
                                    </ul>
                                    <button type="button" class="btn  btn-lg hvr-pulse-grow call-me">  <i class="fas fa-phone"></i><span>Click To Show Number 011472*****</span> </button>

                                </div>
                            </div>
                        </section>

                        <section class="recent-listing">
                            <div class="head">
                                <h4 class="head-title" ><span class="border-head">|</span> RECENT LISTINGS</h4>
                            </div>
                            <div class="bd-example">
                                <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">

                                    <div class="carousel-inner" role="listbox">
                                        <?php
                                        foreach ($recent_ads as $key => $recent_ad) {
                                            $active = $key ==0 ? "active":"";
                                            ?>
                                            <div class="carousel-item <?=$active?>">
                                                <img src="<?= $recent_ad->images_list['0'] ?>"  width="100%" alt="...">
                                                <div class="carousel-caption d-none d-md-block">
                                                    <div class="d-flex justify-content-between">
                                                        <div> <a href="#" title="<?=$recent_ad->name?>" rel="nofollow" tabindex="-1"><?=$recent_ad->name?> for <br><span><?=$recent_ad->price?></span> </a>
                                                        </div>
                                                        <div ><span class="has_featured-tag-1">Featured</span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>

                        </section>


                        <?=$this->element('frontend/cats_list',['categories'=>$main_categories]);?>
                        <section class="cat-offer">
                            <div class="head">
                                <h4 class="head-title"><span class="border-head">|</span> Offers</h4>
                            </div>
                            <ul class="list-group">
                                <li class="list-group-item ">
                                    <div class="parent-offer">
                                        <span class="name-of-cat float-left">Total Bids</span>
                                        <span class="categories-count float-right "> 0</span>
                                    </div>
                                </li>
                                <li class="list-group-item ">
                                    <div class="parent-offer">
                                        <span class="name-of-cat float-left">Highest Bid</span>
                                        <span class="categories-count float-right "> 0</span>
                                    </div>
                                </li>
                                <li class="list-group-item ">
                                    <div class="parent-offer">
                                        <span class="name-of-cat float-left">Lowest Bid</span>
                                        <span class="categories-count float-right "> 0</span>
                                    </div>
                                </li>
                                <li class="list-group-item ">
                                    <div class="parent-offer">
                                        <span class="name-of-cat float-left">Average Bid</span>
                                        <span class="categories-count float-right "> 0</span>
                                    </div>
                                </li>
                            </ul>

                        </section>
                        <section class="cat-safety">
                            <div class="head">
                                <h4 class="head-title"><span class="border-head">|</span> Safety Tips</h4>
                            </div>
                            <ul>
                                <li>Meet seller at a safe location</li>
                                <li>Check the item before you buy</li>
                                <li> Pay only after collecting item</li>
                            </ul>
                        </section>
                        <section class="advertising">
                            <div class="head">
                                <h4 class="head-title"><span class="border-head">|</span> advertising</h4>
                            </div>
                            <img src="<?=ROOT_URL?>/frontend/image/banne.jpg">
                        </section>
                    </div>

                </aside>
            </div>
        </div>
    </div>
</section>
<!--END FOOTER -->
