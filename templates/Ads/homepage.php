<section class="featured-adsx">
    <div class="container">
        <div class="row">


            <?php foreach ($ads as $ad){
               echo $this->element('frontend/block_ad' , ['ad' => $ad]);
            }
            ?>



        </div>
        <div class="d-flex justify-content-lg-center">
            <button type="button" class="btn btn-success btn-lg mt-5 hvr-buzz">Load More</button>
        </div>
    </div>
</section>
