<link rel="stylesheet" href="<?=ROOT_URL?>frontend/css/form.css">
<section id="login" class="p-5 ">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card  p-5 w-50">
                    <h4 class="card-title text-center">LOGIN</h4>
                    <form >
                        <div class="input-group mt-3">
                            <input type="text" class="form-control bg-light" placeholder="Username" >
                            <div class="input-group-append" >
                          <span class="input-group-text">
                            <i class="fas fa-user  " style="color: 315d78;"></i>
                          </span>
                            </div>
                        </div>
                        <div class="input-group mt-4">
                            <input type="password" class="form-control bg-light" placeholder="Password">
                            <div class="input-group-append" >
                          <span class="input-group-text">
                            <i class="fas fa-lock " style="color: 315d78;"></i>
                        </span>
                            </div>
                        </div>
                    </form>
                    <a href="http://" class="mt-4 forget">Forget Password?</a>
                    <button type="button" class="btn btn-danger btn-lg btn-block hvr-pulse-grow mt-4 ">Submit</button>
                </div>
            </div>
        </div>

    </div>
</section>
