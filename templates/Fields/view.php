<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Field $field
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('Edit Field'), ['action' => 'edit', $field->id], ['class' => 'nav-link']) ?></li>
<li><?= $this->Form->postLink(__('Delete Field'), ['action' => 'delete', $field->id], ['confirm' => __('Are you sure you want to delete # {0}?', $field->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Fields'), ['action' => 'index'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('New Field'), ['action' => 'add'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Ads'), ['controller' => 'Ads', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Ad'), ['controller' => 'Ads', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="fields view large-9 medium-8 columns content">
    <h3><?= h($field->name) ?></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($field->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Category') ?></th>
                <td><?= $field->has('category') ? $this->Html->link($field->category->name, ['controller' => 'Categories', 'action' => 'view', $field->category->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Type') ?></th>
                <td><?= h($field->type) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($field->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($field->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($field->modified) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Required') ?></th>
                <td><?= $field->required ? __('Yes') : __('No'); ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Search') ?></th>
                <td><?= $field->search ? __('Yes') : __('No'); ?></td>
            </tr>
        </table>
    </div>
    <div class="row">
        <h4><?= __('Options') ?></h4>
        <?= $this->Text->autoParagraph(h($field->options)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Ads') ?></h4>
        <?php if (!empty($field->ads)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Name') ?></th>
                    <th scope="col"><?= __('Slug') ?></th>
                    <th scope="col"><?= __('Description') ?></th>
                    <th scope="col"><?= __('Image Folder') ?></th>
                    <th scope="col"><?= __('Category Id') ?></th>
                    <th scope="col"><?= __('Location Id') ?></th>
                    <th scope="col"><?= __('User Id') ?></th>
                    <th scope="col"><?= __('Active') ?></th>
                    <th scope="col"><?= __('Statues') ?></th>
                    <th scope="col"><?= __('Type') ?></th>
                    <th scope="col"><?= __('Active Fo Days') ?></th>
                    <th scope="col"><?= __('Featured') ?></th>
                    <th scope="col"><?= __('Views') ?></th>
                    <th scope="col"><?= __('Rate') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($field->ads as $ads): ?>
                <tr>
                    <td><?= h($ads->id) ?></td>
                    <td><?= h($ads->name) ?></td>
                    <td><?= h($ads->slug) ?></td>
                    <td><?= h($ads->description) ?></td>
                    <td><?= h($ads->image_folder) ?></td>
                    <td><?= h($ads->category_id) ?></td>
                    <td><?= h($ads->location_id) ?></td>
                    <td><?= h($ads->user_id) ?></td>
                    <td><?= h($ads->active) ?></td>
                    <td><?= h($ads->statues) ?></td>
                    <td><?= h($ads->type) ?></td>
                    <td><?= h($ads->active_fo_days) ?></td>
                    <td><?= h($ads->featured) ?></td>
                    <td><?= h($ads->views) ?></td>
                    <td><?= h($ads->rate) ?></td>
                    <td><?= h($ads->created) ?></td>
                    <td><?= h($ads->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'Ads', 'action' => 'view', $ads->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'Ads', 'action' => 'edit', $ads->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Form->postLink( __('Delete'), ['controller' => 'Ads', 'action' => 'delete', $ads->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ads->id), 'class' => 'btn btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <?php endif; ?>
    </div>
</div>
