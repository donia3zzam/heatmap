<?php
//pr($hall->floor_image);
$size = getimagesize($hall->floor_image);
$imageH = $size[1];
$imageW = $size[0];


?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>

<canvas id="Canvas" width="<?=$imageW?>" height="<?=$imageH?>" style="
    display: block;
    width: <?=$imageW?>px!important;
    height: <?=$imageH?>px!important;
    "></canvas>
<?= $this->Form->create([]) ?>
<table id="" class="table table-striped">
    <tr>
        <td>sensor name</td>
        <td>position X</td>
        <td>position Y</td>
        <td>file</td>
        <td>
            <a class="btn btn-danger" href="<?= ROOT_URL ?>halls/deletes/<?= $this->request->getParam('pass')[0] ?>">Delete All Old Sensors</a>
        </td>
    </tr>
    <tbody id="markers">

    </tbody>
</table>
<?= $this->Form->button(__('Upload..'),['class'=>'final_save btn btn-primary']) ?>
<a class="btn btn-success" href="<?= ROOT_URL ?>halls/done">Done</a>
<?= $this->Form->end() ?>
<script>
    var $nextButton = 0;
    var clickTheNext = true;
    var fileNumber = 0;
    var hallID = <?= $this->request->getParam('pass')[0] ?>;
    var widthsOfFloor = <?=$imageW?>;
    var HeightOfFloor = <?=$imageH?>;
    var pixelToCMW = widthsOfFloor / widthsOfFloor;
    var pixelToCMH = HeightOfFloor / HeightOfFloor;
    console.log(pixelToCMH);
    console.log(pixelToCMW);
    var canvas = document.getElementById('Canvas');
    var context = canvas.getContext("2d");

    // Map sprite
    var mapSprite = new Image();
    mapSprite.src = "<?=$hall->floor_image?>";
    var counter = 1 ;
    var Marker = function () {
        this.Sprite = new Image();
        this.Sprite.src = "http://www.clker.com/cliparts/w/O/e/P/x/i/map-marker-hi.png"
        this.Width = 12;
        this.Height = 20;
        this.XPos = 0;
        this.YPos = 0;
        this.WPos = 0;
        this.HPos = 0;
        this.id = counter;
        counter++;
    }

    function changeMarker(target,xValue,yValue){
        Markers.forEach(function(marker,index){
            if(marker.id == target){
                Markers[index].XPos = xValue;
                Markers[index].YPos = yValue;
            }
        })
    }

    var Markers = new Array();

    var mouseClicked = function (mouse) {
        // Get corrent mouse coords
        var rect = canvas.getBoundingClientRect();
        var mouseXPos = (mouse.x - rect.left);
        var mouseYPos = (mouse.y - rect.top);

        console.log("Marker added");

        // Move the marker when placed to a better location
        var marker = new Marker();
        marker.XPos = (mouseXPos - (marker.Width / 2)).toFixed(2);
        marker.YPos = (mouseYPos - marker.Height).toFixed(2);
        marker.WPos = marker.XPos * pixelToCMW;
        marker.HPos = marker.YPos * pixelToCMH;
        console.log(marker.WPos);
        console.log(marker.HPos);
        Markers.push(marker);
        create_marker_row(marker);
        setTimeout(function(){
            $('.changer').change(function(){
                $yValue = $(this).parent('td').parent('tr').find('.changery').val();
                $xValue = $(this).parent('td').parent('tr').find('.changerx').val();
                console.log($yValue)
                console.log($xValue)
                $name =  $(this).parent('td').parent('tr').find('.changerx').attr('name');
                $target = $name.replace('[x]','');
                changeMarker($target,$xValue,$yValue);
            })
        })
    }
    function create_marker_row(marker){
        console.log(Markers);
        var table = document.getElementById('markers');
        var tbody = table.getElementsByTagName("tbody");
        Markers.forEach(function(marker){

        })
        var row = table.insertRow(0);

// Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);

        // Add some text to the new cells:
        cell1.innerHTML = "<span class ='classSelectorName'>"+marker.id+"</span>";
        cell2.innerHTML = "<input type='number' class='changerx testClassX changer' name='"+marker.id+"[x]' value='"+marker.XPos+"'>";
        cell3.innerHTML = "<input type='number' class='changery testClassY changer' name='"+marker.id+"[y]' value='"+marker.YPos+"'>";
        cell4.innerHTML = "<input type='file' class='file-upload' name='"+marker.id+"[data]' >" + 
                          "<input type='button' class='button but_upload' value='Upload'>" +
                          "<div id='done' class='done' style='color:green; font-weight: bold; display:none;'>File uploaded successfully...</div>" +
                          "<div id='myDiv'> <img class='loading-image' src='http://167.86.95.202/infosalons/webroot/img/loader.gif' width='50' height='50' style='display:none;'/> </div>" ;
                          uploadListener()
        }

        //Ali's Code
        function uploadListener(){
            $(".but_upload").off('click')
            $(".but_upload").click(function(){
                var fdd = new FormData();
                var files = $('.file-upload')[fileNumber].files;
                console.log(files);
                $time = (files[0].size / 1000000) * 10;
                console.log($time);
                $targetElement = $(this).parent();

                if(files.length > 0 ){
                    console.log(fileNumber);
                    fdd.append('file', files[0]);
                    fdd.append('name', $($targetElement).parent().find('.classSelectorName').text());
                    fdd.append('x', $($targetElement).parent().find('.changerx').val());
                    fdd.append('y', $($targetElement).parent().find('.changery').val());
                    
                    $.ajax({
                        url: '<?=ROOT_URL?>/halls/uploads/' + hallID,
                        type: 'post',
                        data: fdd,
                        contentType: false,
                        processData: false,
                        cache: false,
                        beforeSend: function() {
                            $($targetElement).find(".loading-image").show();
                            // $($targetElement).find(".but_upload").show();
                            // $nextButton = 0;
                            clickTheNext = false;
                        },
                        success: function(){
                            $($targetElement).find(".done").show();
                            $($targetElement).find(".loading-image").hide();
                            // $($targetElement).find(".but_upload").show();
                            $nextButton = $nextButton + 1 ;
                            clickTheNext = true;
                            console.log("success")
                        },
                        error : function(jqXHR, textStatus, errorThrown){
                        },
                    });
                }
                else {
                    alert("Please select a file.");
                }
                var i = 0;
                if (i == 0) {
                    i = 1;
                    var elem = $($targetElement).find('#myBar');
                    // document.getElementById("myBar");
                    var width = 1;
                    var id = setInterval(frame, $time);
                    function frame() {
                        if (width >= 100) {
                            clearInterval(id);
                            i = 0;
                            
                        } else {
                            width++;
                            $(elem).css('width' , width + "%" );
                            // elem.style.width = width + "%";
                        }
                    }
                }
                fileNumber++;
            });
        };
        //End Of Ali's Code

    // Add mouse click event listener to canvas
    canvas.addEventListener("mousedown", mouseClicked, false);

    var firstLoad = function () {
        context.font = "15px Georgia";
        context.textAlign = "center";
    }

    firstLoad();

    var main = function () {
        draw();
    };

    var draw = function () {
        // Clear Canvas
        context.fillStyle = "#000";
        context.fillRect(0, 0, canvas.width, canvas.height);

        // Draw map
        // Sprite, X location, Y location, Image width, Image height
        // You can leave the image height and width off, if you do it will draw the image at default size
        context.drawImage(mapSprite, 0, 0, canvas.width, canvas.height);

        // Draw markers
        for (var i = 0; i < Markers.length; i++) {
            var tempMarker = Markers[i];
            // Draw marker
            context.drawImage(tempMarker.Sprite, tempMarker.XPos, tempMarker.YPos, tempMarker.Width, tempMarker.Height);

            // Calculate postion text
            var markerText = "Postion (X:" + tempMarker.XPos + ", Y:" + tempMarker.YPos ;

            // Draw a simple box so you can see the position
            var textMeasurements = context.measureText(markerText);
            context.fillStyle = "#666";
            context.globalAlpha = 0.7;
            context.fillRect(tempMarker.XPos - (textMeasurements.width / 2), tempMarker.YPos - 15, textMeasurements.width, 20);
            context.globalAlpha = 1;

            // Draw position above
            context.fillStyle = "#000";
            context.fillText(markerText, tempMarker.XPos, tempMarker.YPos);
        }
    };

    setInterval(main, (1000 / 60));

    $('.final_save').click(function(){
        $countOfUpButtons = $('.but_upload').length;
        console.log($countOfUpButtons);
        setInterval(() => {
            if(clickTheNext){
                $('.but_upload:eq('+$nextButton+')').click();
            }
            console.log('test');
        }, 2000);
        return false; 
    })
</script>


<style>
.but_upload{
    display: none;
}
</style>