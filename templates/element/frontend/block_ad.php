<?php
$imageURL = str_replace('http://192.168.1.66/','http://localhost/',$ad->images_list[0]);
?>
<div class=" col-lg-3 col-md-4">
    <div class="card hvr-bounce-in card-islam">
        <img class="card-img-top" src="<?=ROOT_URL?>/timthumb.php?src=<?=$imageURL?>&h=168&zc=1&a=c&w=250"   alt="Card image cap">
        <div class="card-body">
            <a href="<?=ROOT_URL?>/ads/details/<?=$ad->id?>" class="link">
                <div class="cat">
                    <a href="#" class="listing-cat"><?=$ad->category['name']?></a>
                    <span class="cat-seperator fas fa-angle-right"></span>
                    <a href="#" class="listing-cat"><?=$ad->name?></a>
                </div>
                <header class="listing-title">
                    <h2>
                        <a href="#" class="listing-title" title="Group Tours" rel="nofollow"><?=$ad->user['username']?></a>
                    </h2>
                    <p class="listing-location">
                        <i class="fas fa-map-marker-alt"></i>
                        <span class="location">
                                <span itemprop="addressLocality"><?=$ad->location['name']?></span>
                            </span>
                    </p>
                </header>
                <div class="listing-rating grid-rating">
                                 <span class="rating-numbers-empty ">
                                    <i class="far fa-frown-open"></i>
                                </span>
                    <span class="review_rate-empty ">
                                <a href="#" data-toggle="tooltip" title="" data-original-title="Be first to rate" class="rate">Rate Now</a>

                                <span class="right-icon " title="HOP@gmail.com"><i class="far fa-envelope three-icons"></i></span>
                                <span class="right-icon " title="02255788"><i class="fas fa-mobile-alt three-icons"></i></span>
                                     <a href="#" >
                                <span class="right-icon" title="Add To Book Mark">
                                <i class="fas fa-heart three-icons fav"></i>

                                </span>
                                </a>
                                </span>
                </div>
                <div class="details">
                    <span class="currency-symbol">EGP</span>
                    <span class="price"><?=$ad->price?></span>
                    <p class="listing-views">Views: <?=$ad->views?></p>
                </div>
                <a href="<?=ROOT_URL?>/ads/details/<?=$ad->id?>">
                    <button type="button" class="  btn-success btn-sm">Read More</button>
                </a>
            </a>
        </div>
    </div>
</div>
