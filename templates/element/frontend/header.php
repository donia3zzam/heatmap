<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;700;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?=ROOT_URL?>/frontend/css/base.css">
    <link rel="stylesheet" href="<?=ROOT_URL?>/frontend/css/hover-min.css">
    <link rel="stylesheet" href="<?=ROOT_URL?>/frontend/css/premium.css">
    <link rel="stylesheet" href="<?=ROOT_URL?>/frontend/css/commn.css">
    <link rel="stylesheet" href="<?=ROOT_URL?>/frontend/css/home.css">
    <link rel="stylesheet" href="<?=ROOT_URL?>/frontend/css/contact.css">
    <link rel="stylesheet" href="<?=ROOT_URL?>/frontend/css/product.css">
    <link rel="stylesheet" href="<?=ROOT_URL?>/frontend/css/colors.css">


    <title>Home</title>
    <style>
        .btn{
            background-color: #f26628;
            border-color: #f26628;
            color: white;
        }
        .btn:hover{
            background-color: #315d78;
            border-color: #315d78;
        }
        .btn-sm:hover{
            background-color: #f26628 ;
            border-color: #f26628 ;
            color: white;

        }
        .btn-sm{
            background-color: #315d78;
            border-color: #315d78;
            color: white;
        }
        .btn-link{
            width: 100%;
            color: #f26628;
            background-color: #315d78;
            border-color: #315d78;
        }
        .btn-link:hover{
            color: #f26628;
        }
    </style>
</head>
<body>
