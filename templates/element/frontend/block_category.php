<?php
$size = ($widget_size == 12 ) ?2 : 6;
?>
<div class=" col-md-<?=$size?> col-sm-<?=$size?> col-xs-12">
    <div class="card  ">
        <div class="card-body">

            <a href="<?=ROOT_URL?>/categories/list/<?=$category->id?>" class="link">
                <img class="card-img-top" src="<?=$category->image?>" alt="Card image cap" style="width: 150px;height: 150px" width="150">
                <h6 class="card-subtitle m-2 text-center text-muted"><?=$category->name?></h6>
                                <h6 class="categories-count text-center"><?=$category->ads_count?>  <i class="fas fa-plus-circle " data-popup-open="70"></i></h6>
            </a>
        </div>
    </div>
</div>
