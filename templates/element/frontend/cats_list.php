<section class="cat-list">
    <div class="head">
        <h4 class="head-title"><span class="border-head">|</span> CATEGORIES</h4>
    </div>
    <ul class="list-group">
        <?php
        foreach ($categories as $cat){
            ?>
            <li class="list-group-item ">
                <div class="parent-category">
                    <a href="<?=ROOT_URL?>/categories/list/<?=$cat->id?>" title="<?=$cat->name?>">
                                    <span class="cat-icon ">
                                        <img class="directorypress-field-icon" src="<?=$cat->image?>" alt="Real Estate">
                                    </span>
                        <span class="name-of-cat"><?=$cat->name?></span>
                        <span class="categories-count "> (1)</span>
                    </a>

                </div></li>

            <?php
        }
        ?>




    </ul>

</section>
