
<!--Start Footer Section-->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-5 col-xs-12">
                <h4>About US</h4>
                <hr class="hr-footer">
                <p class="footer-text">The HOP-Ads is all time #1 Premium Classified Ads  We know its a perfect choice for your business. Super flexible, Rich with features availability.</p>
                <a href="#" class="btn  btn-success hvr-pulse-grow">Read More</a>
            </div>
            <div class="col-sm-4 col-md-3 col-lg-3">

                <ul>
                    <h4>Quick links</h4>
                    <li class="list-item">
                        <a href="dashboared/myaccount.html">My Dashboard</a>
                        <i class="fas fa-angle-double-right"></i>
                    </li>
                    <li class="list-item">
                        <a href="#">Submit-listing</a>
                        <i class="fas fa-angle-double-right"></i>
                    </li>
                    <li class="list-item">
                        <a href="login.html">Login</a>
                        <i class="fas fa-angle-double-right"></i>
                    </li>
                    <li class="list-item">
                        <a href="register.html">Register</a>
                        <i class="fas fa-angle-double-right"></i>
                    </li>
                </ul>
            </div>
            <div class="col-lg-4 col-md-3 col-xs-12">
                <div class="d-flex flex-row">
                    <div class="p-2 "><a href="product.html"><img src="<?=ROOT_URL?>/frontend/image/footer-image.png" style="width: 100%;"></a></div>
                </div>
            </div>
        </div>

    </div>

</footer>
<div class="sub-footer">
    <div class="container">
        <div class="d-flex justify-content-between">
            <div class="p-2">
                <p class="footer-text">All Copyrights reserved @ 2021 - Design by MACBER</p>
            </div>
            <!--            <div class="p-2">Flex item 2</div>-->
            <div class="p-2">
                <ul class="list-inline">
                    <li class="list-inline-item p-3"> <a href="#"><i class="fab fa-facebook-f"></i></a></li>
                    <li class="list-inline-item p-3"> <a href="#"><i class="fab fa-twitter"></i></a></li>
                    <li class="list-inline-item p-3"> <a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                    <li class="list-inline-item p-3"> <a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                    <li class="list-inline-item p-3"> <a href="#"><i class="fab fa-instagram"></i></a></li>
                </ul>
            </div>
        </div>
    </div>

</div>


<style>
    .model_categories{

    }
    .model_categories li {
        list-style: none;
        width: 190px;
        display: inline-block;
        text-align: center;
    }
</style>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Select Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="model_categories">
                    <?php foreach ($main_categories as $cat):
                        ?>
                        <li><a href="<?=ROOT_URL?>ads/create?cat_id=<?=$cat->id?>"><img src="<?=$cat->image?>"></a> <span><?=$cat->name?></span></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
<!--                <button type="button" class="btn btn-primary">Save changes</button>-->
            </div>
        </div>
    </div>
</div>
<!--END Footer Section-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

<script>
    $(document).ready(function(){
        $('#BrandList').change(function () {
            $id = $(this).val();
            $.ajax({
               url:"<?=ROOT_URL?>categories/getSubCat/"+$id,
                context: document.body
            }).done(function(data) {
                $('#exampleFormControlSelect1').find('option').remove();
                data.categories.forEach(function(row){
                    $('#exampleFormControlSelect1').append("<option value='"+row.id+"'>"+row.name+"</option>");
                });
            });
        })
    })
</script>
</body>
</html>
