
<!--Head-Search Section-->
<section class="head-search">
    <div class="container">
        <div class="row">
            <div class="col">
                <nav class="mt-3">
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <h4 class="keyword text-white">Search</h4>
                        <a class="nav-link active ml-5" id="nav-real-state-tab" data-toggle="tab" href="#nav-real-state" role="tab" aria-controls="nav-real-state" aria-selected="true">Real State</a>
                        <a class="nav-link" id="nav-vehicles-tab" data-toggle="tab" href="#nav-vehicles" role="tab" aria-controls="nav-vehicles" aria-selected="false">Vehicles</a>
                        <a class="nav-link" id="nav-watercraft-tab" data-toggle="tab" href="#nav-watercraft" role="tab" aria-controls="nav-watercraft" aria-selected="false">Watercraft</a>
                        <a class="nav-link" id="nav-golf-tab" data-toggle="tab" href="#nav-golf" role="tab" aria-controls="nav-golf" aria-selected="false">Golf Car</a>
                    </div>
                </nav>
                <div class="tab-content mt-4" id="nav-tabContent">
                    <div class="tab-pane fade show active " id="nav-real-state" role="tabpanel" aria-labelledby="nav-real-state-tab">
                        <form action="<?=ROOT_URL?>ads/listing/" method="get">
                        <div class="row ">
                            <div class="col-lg-10">
                                <div class="form-row">
                                    <div class="form-group col">
                                        <input type="text" name="keyword" class="form-control" id="inputPrice" placeholder="Enter keyword">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-row">
                                    <div class="form-group col">
                                        <button type="submit" class="searching-button btn ">Search</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="nav-vehicles" role="tabpanel" aria-labelledby="nav-vehicles-tab">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <?=$this->Form->control('Choose Brand',['type' => 'select' , 'id'=>'BrandList' ,  'options' => $brands,'empty' =>"Select" ] )?>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1" class="text-white">Choose the model</label>
                                    <select class="form-control" id="exampleFormControlSelect1">
                                        <option>Select</option>
                                        <option>BMW</option>
                                        <option>Opel</option>
                                        <option>Mercedes</option>
                                        <option>KIA</option>
                                        <option>Hyundai</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <input type="number" class="form-control" id="inputPrice" placeholder="Price from">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="number" class="form-control" id="inputPrice" placeholder="Price to">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <input type="number" class="form-control" id="inputYear" placeholder="Year from">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="number" class="form-control" id="inputYear" placeholder="Year to">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <input type="number" class="form-control" id="inputText" placeholder="Capacitance from">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="number" class="form-control" id="inputText" placeholder="Capacitance to">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <button type="submit" class="searching-button btn float-right">Search</button>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-watercraft" role="tabpanel" aria-labelledby="nav-watercraft-tab">
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-row">
                                    <div class="form-group col">
                                        <input type="text" class="form-control" id="inputPrice" placeholder="Enter KeyWord">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-row">
                                    <div class="form-group col">
                                        <button type="submit" class="searching-button btn ">Search</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-golf" role="tabpanel" aria-labelledby="nav-golf-tab">
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-row">
                                    <div class="form-group col">
                                        <input type="text" class="form-control" id="inputPrice" placeholder="Enter KeyWord">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-row">
                                    <div class="form-group col">
                                        <button type="submit" class="searching-button btn ">Search</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--END Head-Search-->

