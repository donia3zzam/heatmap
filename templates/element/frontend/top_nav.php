
<!--NavBar Section-->
<nav class="navbar navbar-expand-lg p-4   ">
    <a class="navbar-brand" href="<?=ROOT_URL?>/"> <img src="<?=ROOT_URL?>/frontend/image/Untitled-2.png"> </a>
    <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fas fa-align-justify"></i>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto ">
            <li class="nav-item  active">
                <a class="nav-link hvr-pulse-grow" href="<?=ROOT_URL?>">Home <span class="sr-only">(current)</span></a>
            </li>

            <!-- <li class="nav-item">
                <a class="nav-link  hvr-pulse-grow" href="listning.html">Listing</a>
            </li> -->
            <!-- <li class="nav-item ">
                <a class="nav-link  hvr-pulse-grow" href="categories.html">Categories</a>
            </li> -->
            <!-- <li class="nav-item">
                <a class="nav-link  hvr-pulse-grow" href="blog.html">Blog</a>
            </li> -->
            <li class="nav-item">
                <a class="nav-link  hvr-pulse-grow" href="<?=ROOT_URL?>/pages/show/1">Contact</a>
            </li>
            <li class="nav-item">
                <a href="<?=ROOT_URL?>ads/create" class="btn btn-danger  hvr-pulse-grow" data-toggle="modal" data-target="#exampleModal">Post Your Ad</a>
            </li>
            <li class="nav-item">
                <a class="nav-link  hvr-pulse-grow" href="<?=ROOT_URL?>login">Login</a>
            </li>
            <li class="nav-item">
                <a class="nav-link  hvr-pulse-grow" href="<?=ROOT_URL?>register">REGISTER</a>
            </li>
        </ul>
    </div>
</nav>
<!--End NavBar Section-->
