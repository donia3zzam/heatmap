<!-- /.content-wrapper -->
<footer class="main-footer">
   <?=PROJECT_COPY?>
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> <?=PROJECT_VERSION?>
    </div>
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!-- jQuery -->
<script src="<?=ROOT_URL?>plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?=ROOT_URL?>plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?=ROOT_URL?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?=ROOT_URL?>plugins/select2/js/select2.full.js"></script>
a
<!-- ChartJS -->
<script src="<?=ROOT_URL?>plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?=ROOT_URL?>plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="<?=ROOT_URL?>plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="<?=ROOT_URL?>plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?=ROOT_URL?>plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?=ROOT_URL?>plugins/moment/moment.min.js"></script>
<script src="<?=ROOT_URL?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?=ROOT_URL?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!--Bootstrab switch-->
<script src="<?=ROOT_URL?>plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>

<!-- Summernote -->
<script src="<?=ROOT_URL?>plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?=ROOT_URL?>plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=ROOT_URL?>dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?=ROOT_URL?>dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=ROOT_URL?>dist/js/demo.js"></script>
<?php
echo $this->fetch('scriptBottom');
echo $this->fetch('scriptcode');
?>
</body>
</html>
