<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.bootstrap4.min.css">
<link rel="stylesheet" href="<?=ROOT_URL?>plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?=ROOT_URL?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">

<?php
echo $this->Html->script(ROOT_URL.'plugins/datatables/jquery.dataTables.min.js">', ['block' => 'scriptBottom']);
echo $this->Html->script(ROOT_URL.'plugins/datatables-bs4/js/dataTables.bootstrap4.min.js">', ['block' => 'scriptBottom']);
echo $this->Html->script(ROOT_URL.'plugins/datatables-responsive/js/dataTables.responsive.min.js">', ['block' => 'scriptBottom']);
echo $this->Html->script(ROOT_URL.'plugins/datatables-responsive/js/responsive.bootstrap4.min.js">', ['block' => 'scriptBottom']);
echo $this->Html->script('https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js', ['block' => 'scriptBottom']);
echo $this->Html->script('https://cdn.datatables.net/buttons/1.6.1/js/buttons.bootstrap4.min.js', ['block' => 'scriptBottom']);


echo $this->Html->script('https://cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js', ['block' => 'scriptBottom']);
echo $this->Html->script('https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js', ['block' => 'scriptBottom']);
echo $this->Html->script('https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js', ['block' => 'scriptBottom']);
echo $this->Html->script('https://cdn.datatables.net/buttons/1.6.1/js/buttons.bootstrap.min.js', ['block' => 'scriptBottom']);
echo $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js', ['block' => 'scriptBottom']);
echo $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.62/pdfmake.js', ['block' => 'scriptBottom']);
echo $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.62/vfs_fonts.js', ['block' => 'scriptBottom']);
echo $this->Html->script('https://cdn.datatables.net/fixedheader/3.1.2/js/dataTables.fixedHeader.min.js', ['block' => 'scriptBottom']);
echo $this->Html->script('https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js', ['block' => 'scriptBottom']);

?>

<table border="0" cellspacing="5" cellpadding="5" style="display: none">
    <tbody>
    <tr>
        <td>Minimum Date:</td>
        <td><input name="min" id="min" type="text"></td>
    </tr>
    <tr>
        <td>Maximum Date:</td>
        <td><input name="max" id="max" type="text"></td>
    </tr>
    </tbody>
</table>

<style>

    @keyframes flickerAnimation {
        0%   { opacity:1; }
        50%  { opacity:0; }
        100% { opacity:1; }
    }
    @-o-keyframes flickerAnimation{
        0%   { opacity:1; }
        50%  { opacity:0; }
        100% { opacity:1; }
    }
    @-moz-keyframes flickerAnimation{
        0%   { opacity:1; }
        50%  { opacity:0; }
        100% { opacity:1; }
    }
    @-webkit-keyframes flickerAnimation{
        0%   { opacity:1; }
        50%  { opacity:0; }
        100% { opacity:1; }
    }
    .animate-flicker {
        -webkit-animation: flickerAnimation .9s forwards;
        -moz-animation: flickerAnimation .9s forwards;
        -o-animation: flickerAnimation .9s forwards;
        animation: flickerAnimation .9s forwards;
    }


    .dt-buttons{

    }
    .dt-buttons .btn-group{}
    .dt-buttons .example1_filter{
        width: 200px;
    }
    .dt-buttons .example1_length{

    }
    .buttons-columnVisibility  a:hover {
        background: #F5F5F5;
    }
    .buttons-columnVisibility.active  a{
        color: #007bff;
        text-decoration: none;

    }
    .buttons-columnVisibility  a{
        color: #007bff;
        text-decoration: line-through;
        padding: 5px;
        display: block;
        text-transform: capitalize;
        border-bottom: 1px solid #ddd;
    }


    .dt-buttons {
        margin-bottom: 10px;
    }
    .dt-buttons.btn-group{
        float: none;
        margin-right: 2%;
        clear: both;
        width: 100%;
    }
    .dataTables_filter {
        float: left;
        margin-top: 4px;
        margin-right: 2%;
        text-align: left;
    }
    .dataTables_info {
        float: right;
    }
    .dataTables_length{
        float: right;
        margin-top: 4px;
        margin-left: 2%;
    }
    .emp{
        background: #fff;
        position: fixed;
        z-index: 1;
        padding: 50px;
        right: -30%;
        bottom: 0;
        transition: all 0.7s ease-in-out;
    }
    .show-emp{
        right: 0px;
        bottom: 0;
    }


    .float{
        position:fixed;
        width:60px;
        height:60px;
        bottom:40px;
        right:40px;
        background-color:#0C9;
        color:#FFF;
        border-radius:50px;
        text-align:center;
        box-shadow: 2px 2px 3px #999;
    }

    .my-float{
        margin-top:22px;
    }

    .currentChange{
        transition: all 0.7s ease-in-out;
        background: #da2839!important;

    }
</style>
<a href="#" class="float">
    <i class="fa fa-filter my-float"></i>
</a>


<script>
    function selectedCallback($rows){
        $total = $rows.length;
        console.log($rows);
        var $ids = '';
        for ($x = 0; $x < $total; $x++){
            $currentRow = $rows[$x];
            console.log($currentRow);
            $id = $currentRow[0];
            $ids += $id +',';

        }
        console.log($ids);
        window.open("<?=ROOT_URL?>orders/newprintall?ids="+$ids , "_blank");

        // console.log();
    }
</script>
<?php $this->Html->scriptStart(['block' => 'scriptcode']); ?>
<!--<script>-->
function extractContent(s) {
var span = document.createElement('span');
span.innerHTML = s;
return span.textContent || span.innerText;
};

    $(document).ready(function() {
        $('.float').click(function(){
            console.log('clicked');
            $('.emp').addClass('show-emp');
            $('.closeit').click(function(){
                $('.emp').removeClass('show-emp');
            });


        })
        //
        // $('#example1 thead tr').clone(true).appendTo( '.testem thead' );
        // $('#example1 thead tr:eq(1) th').each( function (i) {
        //     var title = $(this).text();
        //     $(this).html( '<input type="text" placeholder="Search '+title+'" style="width: 100px"/>' );
        //
        //     $( 'input', this ).on( 'keyup change', function () {
        //         if ( table.column(i).search() !== this.value ) {
        //             table
        //                 .column(i)
        //                 .search( this.value )
        //                 .draw();
        //         }
        //     } );
        // } );



        $('#reservationtime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
        format: 'MM/DD/YYYY hh:mm A'
        }
        }, function(start, end, label) {
            $('#min').val(start.format('MM/DD/YYYY') );
            $('#max').val(end.format('MM/DD/YYYY') );

            table.draw();
        });
        document.title='TCE Orders';
        // DataTable initialisation
        var table  = $('#example1').DataTable(
            {
                "dom": '<"testme"<"emp">fl>Btip',
                "paging": true,
                "autoWidth": true,
                "fixedHeader": false,
                "select": true,
                "order": [[ 0, "desc" ]],
                "buttons": [
                    'selectAll',
                    'selectNone',
                    'colvis',
                    'csvHtml5',
                    'excelHtml5',
                    'pdfHtml5',
                    'print',
                    {
                    text: 'Print all selected',
                    action: function () {
                        var rows = table.rows( { selected: true } ).data();
                        selectedCallback(rows);
                    }}
                ],
                initComplete: function () {
                    console.log('inited');
                    $ind =0;
                    $lastRecord = this.api().columns()[0].length - 1 ;
                    this.api().columns().every( function () {
                        if($ind < $lastRecord){
                            var column = this;
                            $label = $('#example1 thead tr:eq(0) th:eq('+$ind+')').text();
                            var select = $('<select style="width: 300px" class="select2bs4 form-control "><option value="">'+$label+'</option></select>')
                                .appendTo( $('.emp ') )
                                .on( 'change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );

                                    column
                                        .search( val ? '^'+val+'$' : '', true, false )
                                        .draw();
                                } );
                            column.data().unique().sort().each( function ( d, j ) {
                                $newD  =  extractContent(d)


                                select.append( '<option value="'+$newD+'">'+$newD+'</option>' )
                            } );
                            $ind++;
                        }
                    } );
                    setTimeout(function(){
                        $('.select2bs4').select2({
                            theme: 'bootstrap4'
                        })
                    },50);
                    $('<button class="btn btn-danger closeit">Close Filter</button>').appendTo( $('.emp ') );

                }


            }
        );

        $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                var min = $('#min').datepicker("getDate");
                var max = $('#max').datepicker("getDate");
                var startDate = new Date(data[7]);
                if (min == null && max == null) { return true; }
                if (min == null && startDate <= max) { return true;}
                if(max == null && startDate >= min) {return true;}
                if (startDate <= max && startDate >= min) { return true; }
                return false;
            }
        );


        $("#min").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
        $("#max").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });

        // Event listener to the two range filtering inputs to redraw on input
        $('#min, #max').change(function () {
            table.draw();
        });

    });



<!--</script>-->
<?php $this->Html->scriptEnd(); ?>
