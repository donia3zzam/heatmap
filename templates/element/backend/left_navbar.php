<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-light-info elevation-4 sidebar-dark-purple">
    <!-- Brand Logo -->
    <a href="<?=ROOT_URL?>" class="brand-link">
        <img src="<?=ROOT_URL?>dist/img/AdminLTELogo.png" alt="Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">

        <span class="brand-text font-weight-light"><?=SETTING_KEY['global']['site_settings_']['site_name'];?></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->


        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <?php
                    echo $this->Treelist->generateUls($Array_for_menu);
                if($_SESSION['Auth']['User']['group_id'] == 1){

                ?>
<!---->
<!--                <li class="nav-item has-treeview menu-open">-->
<!--                    <a href="#" class="nav-link active">-->
<!--                        <i class="nav-icon fas fa-tachometer-alt"></i>-->
<!--                        <p>-->
<!--                            Settings-->
<!--                            <i class="right fas fa-angle-left"></i>-->
<!--                        </p>-->
<!--                    </a>-->
<!--                    <ul class="nav nav-treeview">-->
<!---->
<!--                        --><?php
//                            echo $this->Treelist->generateSettingMenu($Settings_menu);
//
//                        ?>
<!--                    </ul>-->
<!--                </li>-->
                <?php } ?>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>

<!-- Content Wrapper. Contains page content -->
