<!-- Argon Scripts -->
<!-- Core -->
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script src="<?=ROOT_URL?>/aragon/assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="<?=ROOT_URL?>/aragon/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="<?=ROOT_URL?>/aragon/assets/vendor/js-cookie/js.cookie.js"></script>
<script src="<?=ROOT_URL?>/aragon/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
<script src="<?=ROOT_URL?>/aragon/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
<!-- Optional JS -->
<script src="<?=ROOT_URL?>/aragon/assets/vendor/chart.js/dist/Chart.min.js"></script>
<script src="<?=ROOT_URL?>/aragon/assets/vendor/chart.js/dist/Chart.extension.js"></script>
<!-- Argon JS -->
<script src="<?=ROOT_URL?>/aragon/assets/js/argon.js?v=1.1.0"></script>
<!-- Demo JS - remove this in your project -->
<script src="<?=ROOT_URL?>/aragon/assets/js/demo.min.js"></script>
<?php
echo $this->fetch('scriptBottom');
echo $this->fetch('scriptcode');
?>
</body>

</html>
