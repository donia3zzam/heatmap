<div class="row">
<?php
echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'yellow',
    'count' => '50',
    'title' => 'New Users',
    'icon' => 'users',
    'sub_msg' => 'More Info',
]);
echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'cyan',
    'count' => '120',
    'title' => 'New Something',
    'icon' => 'users',
    'sub_msg' => 'More Info',
]);
echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'orange',
    'count' => '120',
    'title' => 'New Something',
    'icon' => 'users',
    'sub_msg' => 'More Info',
]);
echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'blue',
    'count' => '120',
    'title' => 'New Something',
    'icon' => 'users',
    'sub_msg' => 'More Info',
]);
echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'dark',
    'count' => '120',
    'title' => 'New Something',
    'icon' => 'users',
    'sub_msg' => 'More Info',
]);
echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'yellow',
    'count' => '120',
    'title' => 'New Something',
    'icon' => 'users',
    'sub_msg' => 'More Info',
]);
echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'light',
    'count' => '120',
    'title' => 'New Something',
    'icon' => 'users',
    'sub_msg' => 'More Info',
]);
echo $this->element('Utils.backend/widgets/count_block',[
    'color' => 'gray',
    'count' => '120',
    'title' => 'New Something',
    'icon' => 'users',
    'sub_msg' => 'More Info',
]);

?>
</div>


<div class="row">
    <section class="col-lg-7 connectedSortable">
    <?php
    echo $this->element('Utils.backend/widgets/chart1',[
        'canv_id' => 'chartscanv',
        'count' => '120',
        'title' => 'New Something',
        'icon' => 'users',
        'sub_msg' => 'More Info',
    ]);

    ?>
    </section>
    <section class="col-lg-5 connectedSortable">
        <?php

        echo $this->element('Utils.backend/widgets/h_count_block',[
            'color' => 'yellow',
            'count' => '120',
            'title' => 'New Something',
            'icon' => 'users',
            'sub_msg' => 'More Info',
        ]);
        echo $this->element('Utils.backend/widgets/h_count_block',[
            'color' => 'light',
            'count' => '120',
            'title' => 'New Something',
            'icon' => 'users',
            'sub_msg' => 'More Info',
        ]);
        echo $this->element('Utils.backend/widgets/h_count_block',[
            'color' => 'gray',
            'count' => '120',
            'title' => 'New Something',
            'icon' => 'users',
            'sub_msg' => 'More Info',
        ]);
        echo $this->element('Utils.backend/widgets/h_count_block',[
            'color' => 'cyan',
            'count' => '120',
            'title' => 'New Something',
            'icon' => 'users',
            'sub_msg' => 'More Info',
        ]);
        echo $this->element('Utils.backend/widgets/h_count_block',[
            'color' => 'red',
            'count' => '120',
            'title' => 'New Something',
            'icon' => 'users',
            'sub_msg' => 'More Info',
        ]);


        ?>
    </section>


</div>

<div class="row">
    <?php
        echo $this->element('Utils.backend/widgets/data_table',[
            'color' => 'dark',
            'size' => '8',
            'count' => '120',
            'title' => 'New Something',
            'icon' => 'users',
            'sub_msg' => 'More Info',

        ]);
        echo $this->element('Utils.backend/widgets/data_table',[
            'color' => 'light',
            'size' => 4,
            'count' => '120',
            'title' => 'New Something',
            'icon' => 'users',
            'sub_msg' => 'More Info',

        ]);
    ?>
</div>
