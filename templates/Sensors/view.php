<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sensor $sensor
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('Edit Sensor'), ['action' => 'edit', $sensor->id], ['class' => 'nav-link']) ?></li>
<li><?= $this->Form->postLink(__('Delete Sensor'), ['action' => 'delete', $sensor->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sensor->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Sensors'), ['action' => 'index'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('New Sensor'), ['action' => 'add'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('List Events'), ['controller' => 'Events', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Event'), ['controller' => 'Events', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Halls'), ['controller' => 'Halls', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Hall'), ['controller' => 'Halls', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Points'), ['controller' => 'Points', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Point'), ['controller' => 'Points', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="sensors view large-9 medium-8 columns content">
    <h3><?= h($sensor->name) ?></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($sensor->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Event') ?></th>
                <td><?= $sensor->has('event') ? $this->Html->link($sensor->event->name, ['controller' => 'Events', 'action' => 'view', $sensor->event->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Hall') ?></th>
                <td><?= $sensor->has('hall') ? $this->Html->link($sensor->hall->name, ['controller' => 'Halls', 'action' => 'view', $sensor->hall->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($sensor->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Location X') ?></th>
                <td><?= $this->Number->format($sensor->location_x) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Location Y') ?></th>
                <td><?= $this->Number->format($sensor->location_y) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($sensor->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($sensor->modified) ?></td>
            </tr>
        </table>
    </div>
    <div class="related">
        <h4><?= __('Related Points') ?></h4>
        <?php if (!empty($sensor->points)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Mac') ?></th>
                    <th scope="col"><?= __('At Time') ?></th>
                    <th scope="col"><?= __('Distance') ?></th>
                    <th scope="col"><?= __('Event Id') ?></th>
                    <th scope="col"><?= __('Sensor Id') ?></th>
                    <th scope="col"><?= __('Hall Id') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($sensor->points as $points): ?>
                <tr>
                    <td><?= h($points->id) ?></td>
                    <td><?= h($points->mac) ?></td>
                    <td><?= h($points->at_time) ?></td>
                    <td><?= h($points->distance) ?></td>
                    <td><?= h($points->event_id) ?></td>
                    <td><?= h($points->sensor_id) ?></td>
                    <td><?= h($points->hall_id) ?></td>
                    <td><?= h($points->created) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'Points', 'action' => 'view', $points->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'Points', 'action' => 'edit', $points->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Form->postLink( __('Delete'), ['controller' => 'Points', 'action' => 'delete', $points->id], ['confirm' => __('Are you sure you want to delete # {0}?', $points->id), 'class' => 'btn btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <?php endif; ?>
    </div>
</div>
